﻿using NUnit.Framework;
using HKeInvestWebApplication.ClientOnly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using HKeInvestWebApplication.Code_File;

namespace HKeInvestWebApplication.ClientOnly.Tests
{
    [TestFixture]
    public class ClientSecurityHoldingsDetailsTests
    {
        ExternalFunctions myExternalFunctions;
        [OneTimeSetUp]
        public void setUp()
        {
            myExternalFunctions = new ExternalFunctions();
        }
        [TestCase("EUR")]
        [TestCase("HKD")]
        [TestCase("JPY")]
        [TestCase("GBP")]
        [TestCase("USD")]
        public void ddlCurrency_SelectedIndexChangedTest(string toCurrency)
        {
            setUp();
            decimal rate = myExternalFunctions.getCurrencyRate(toCurrency);
            Assert.That(rate, Is.GreaterThan(0));
        }
    }
}