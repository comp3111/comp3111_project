﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using HKeInvestWebApplication.Models;
using System.Data;
using System.Data.SqlClient;
using HKeInvestWebApplication.Code_File;



namespace HKeInvestWebApplication.Account
{
    public partial class Register : Page
    {

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            HKeInvestData myHKeInvestData = new HKeInvestData();
            string username = UserName.Text.Trim().ToLower(), // because the userName is required to be case insensitive
                   password = Password.Text.Trim(),
                   firstname = FirstName.Text.Trim(),
                   lastname = LastName.Text.Trim(),
                   hkid = HKID.Text.Trim(),
                   birth = DateOfBirth.Text.Trim(),
                   accountNumber = AccountNumber.Text.Trim();

            string sql = "select * from [Client] where firstName='" + firstname + "'and lastName='" + lastname + "'and HKIDPassportNumber='" + hkid + "' and dateOfBirth=convert(date,'" + birth + "')";
            DataTable table = myHKeInvestData.getData(sql);
            if(table == null || table.Rows.Count == 0)
            {
                // info is wrong
                ErrorMessage.Text = "Input data is incorrect.";
                return;
            } else
            {
                // continue to create user
                var user = new ApplicationUser() { UserName = UserName.Text, Email = Email.Text };
                IdentityResult result = manager.Create(user, Password.Text);
                if(result.Succeeded)
                {
                    // create success
                    // update userName in database
                    SqlTransaction trans = myHKeInvestData.beginTransaction();
                    sql = "update [Account] set userName='" + username + "' where accountNumber='" + accountNumber + "'";
                    myHKeInvestData.setData(sql, trans);
                    myHKeInvestData.commitTransaction(trans);

                    // then assign role
                    IdentityResult roleResult = manager.AddToRole(user.Id, "Client");
                    if (!roleResult.Succeeded)
                    {
                        ErrorMessage.Text = roleResult.Errors.FirstOrDefault();
                        return;
                    }
                    // then send confirmation email
                    HKeInvestCode code = new HKeInvestCode();
                    code.sendEmail(Email.Text.Trim(), "HKeInvest Account Confirmation", "Dear Client, your account is created. Thank you for using HKeInvest.");

                    // Sign in, done
                    signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                }else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                    return;
                }
            }
        }
        private void AddClientRecord(string username)
        {
            string firstname = FirstName.Text.Trim();
            string lastname = LastName.Text.Trim();
            string hkid = HKID.Text.Trim();
            string birth = DateOfBirth.Text.Trim();
            string email = Email.Text.Trim();

            int x = 0;
            string newno = x.ToString();


            string sql3 = lastname.Substring(0, 2);
            string sql4 = (sql3 + newno).Trim();

            string accountNumber = newno.Trim();


            string balance = "20000.00";
            HKeInvestData myHKeInvestData = new HKeInvestData();
            SqlTransaction trans = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData("insert into [Account] ([userName] , [accountNumber] , [balance]) values ('" + username + "','" + accountNumber + "','" + balance + "')", trans);
            myHKeInvestData.setData("insert into [Client] ([firstName] , [lastName] , [HKIDPassportNumber] , [dateOfBirth] , [email] , [accountNumber]) values ('" + firstname + "','" + lastname + "','" + hkid + "','" + birth + "','" + email + "','" + accountNumber + "')", trans);
            myHKeInvestData.commitTransaction(trans);
        }



        protected void cvAccountNumber_ServerValidate(object source, System.Web.UI.WebControls.ServerValidateEventArgs args)
        {
            string account_number = AccountNumber.Text.Trim();
            string last_name = LastName.Text.Trim();
            string lastname = last_name.ToString().ToUpper();
            char first_letter;
            char second_letter;

            if (lastname != "")
            {
                first_letter = lastname[0];
                second_letter = lastname[1];

                if (account_number.Contains(first_letter) == false || (account_number.Contains(first_letter) && account_number.Contains(second_letter)) == false)
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";
                }

            }
        }
    }
}