﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.ClientOnly
{
    public partial class Alert : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        string username;
        string accountNumber;
        string sql = "";
        // for requirement 6a,6c,6d
        string[] colName = { "Account balance(HKD)", "Bond value (HKD)", "Stock value (HKD)", "unit trust value (HKD)", "Total value(HKD)" };
        string[] colsfield = { "balance", "bond", "stock", "unit trust", "total" };
        protected void Page_Load(object sender, EventArgs e)
        {
            username = Context.User.Identity.GetUserName();
            string sqlac = "select accountNumber from Account where userName='" + username + "'";

            DataTable table = myHKeInvestData.getData(sqlac);
            foreach (DataRow row in table.Rows)
            {
                accountNumber = row["accountNumber"].ToString();
            }

            // load the alerts set by the user right after page load
            loadAccountAlerts();
            // load the security holdings by the user
            loadSecurityHoldings();
        }
        // why??
        /*
        protected void gvalertrecord_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable orderTable = myHKeInvestCode.getActiveAlertOfAccount(accountNumber);
            gvalertrecord.DataSource = orderTable;
            if (orderTable == null)
            {
                Label1.Visible = true;
                addalert.Visible = true;
            }
            else
            {
                Label1.Visible = false;
                return;
            }
        }
        */
        protected void addalert_Click(object sender, EventArgs e)
        {
            Button addalert = (Button)sender;
            addalert.Enabled = false;
            GridView1.Visible = true;
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            code.Text = GridView1.SelectedRow.Cells[1].Text.Trim();
        }
        protected void submit_Click(object sender, EventArgs e)
        {
            if (hasSameSecurityAdded(code.Text.Trim(), accountNumber))
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = "You have added an alert for the same security already.";
                return;
            }
            string sql = String.Format("insert into [Alert] (accountNumber,code, lowvalue, highvalue) values ('{0}','{1}','{2}','{3}')", accountNumber,code.Text.Trim(), highvalue.Text.Trim(),lowvalue.Text.Trim());
            SqlTransaction t = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);

            loadAccountAlerts();
        }
        
        private void loadAccountAlerts()
        {
            gvAlerts.Visible = false;
            Label1.Visible = false;

            if (accountNumber == null || accountNumber.Length == 0) return;
            string sql = String.Format("select code,lowvalue,highvalue from [Alert] where accountNumber='{0}'", accountNumber);
            DataTable table = myHKeInvestData.getData(sql);
            if(table == null || table.Rows.Count == 0)
            {
                // ... get a better name please:/
                Label1.Visible = true;
                return;
            }
            gvAlerts.Visible = true;
            gvAlerts.DataSource = table;
            gvAlerts.DataBind();
        }

        private void loadSecurityHoldings()
        {
            GridView1.Visible = false;
            if (accountNumber == null || accountNumber.Length == 0) return;
            string sql = String.Format("select type,code,name,shares,base from [SecurityHolding] where accountNumber='{0}'", accountNumber);
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0)
            {
                // ... get a better name please:/
                Label1.Text = "You have no security holdings.";
                Label1.Visible = true;
                return;
            }

            GridView1.Visible = true;
            GridView1.DataSource = table;
            GridView1.DataBind();
        }

        // tells what to do when user click on the alert shown on the alert summary
        // i.e. remove it

        protected void gvAlerts_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

            // finally..
            // refresh page
            loadAccountAlerts();
        }

        private bool hasSameSecurityAdded(string code, string accountNumber)
        {
            string sql = String.Format("select * from [Alert] where code='{0}' and accountNumber='{1}'",code,accountNumber);
            DataTable t = myHKeInvestData.getData(sql);
            if (t == null) return false;
            return (t.Rows.Count > 0);
        }
        protected void gvAlerts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = gvAlerts.Rows[e.RowIndex];
            string code = row.Cells[1].Text.Trim();
            string lv = row.Cells[2].Text.Trim();
            string hv = row.Cells[3].Text.Trim();

            // this works :p
            string sql = String.Format("delete from [Alert] where accountNumber='{0}'and code='{1}' and lowvalue='{2}'and highvalue='{3}'",accountNumber,code,lv,hv);

            SqlTransaction t = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);

            loadAccountAlerts();


        }
    }
}