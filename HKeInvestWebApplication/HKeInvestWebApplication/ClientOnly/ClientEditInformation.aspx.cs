﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HKeInvestWebApplication.Code_File;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.ClientOnly
{
 
    public partial class ClientEditInformation : System.Web.UI.Page
    {
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestData myHKeInvestData = new HKeInvestData();

        List<Control> accountControlIDs = new List<Control>();
        List<Control> clientControlIDs = new List<Control>();

        string selectedAccountNumber = null;
        string firstName = null, lastName = null, accountNumber = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            // group all controls to their places for easier maipulation at later times

            accountControlIDs.Add(Password);

            clientControlIDs.Add(FirstName);
            clientControlIDs.Add(LastName);
            clientControlIDs.Add(Email);
            clientControlIDs.Add(Building);
            clientControlIDs.Add(Street);
            clientControlIDs.Add(District);
            clientControlIDs.Add(home_phone);
            clientControlIDs.Add(home_fax);
            clientControlIDs.Add(business_phone);
            clientControlIDs.Add(mobile_phone);
            clientControlIDs.Add(country_of_citizenship);
            clientControlIDs.Add(country_of_residence);
            clientControlIDs.Add(ddlExperience);
            clientControlIDs.Add(ddlIncome);
            clientControlIDs.Add(ddlNetworth);
            clientControlIDs.Add(ddlObjctivesver);
            clientControlIDs.Add(ddlStatus);

            // load accountNumber
            string sql = String.Format("select accountNumber from [Account] where userName='{0}'", Context.User.Identity.GetUserName());
            DataTable table = myHKeInvestData.getData(sql);
            if(table == null || table.Rows.Count == 0)
            {
                // then this is really strange. Just leave it here.
                return;
            }
            accountNumber = (table.Rows[0]["accountNumber"] as string).Trim();

            sql = String.Format("select firstName,lastName from [Client] where accountNumber='{0}'", accountNumber);
            table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0)
            {
                // this is another strange thing.
                return;
            }
            // load first name ,last name 
            // actually having the user name is not enough to identify who the client is when there are multiple holders of the same account.
            firstName = (table.Rows[0]["firstName"] as string).Trim();
            lastName = (table.Rows[0]["lastName"] as string).Trim();

            sql = String.Format("select * from [Client] where accountNumber='{0}' and firstName='{1}' and lastName='{2}'", accountNumber, firstName, lastName);
            table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0)
            {
                // perhaps the connection is lost
                return;
            }

            // load the table to UI
            loadDataTable(table.Rows[0]);

        }

        protected void AccountNumber_TextChanged(object sender, EventArgs e)
        {
            ErrorMessage.Visible = false;
            InfoPanel.Visible = false;
            string sql = "select firstName,lastName from [Client] where accountNumber='" + accountNumber + "'";
            DataTable table = myHKeInvestData.getData(sql);
            if(table == null || table.Rows.Count == 0)
            {
                ErrorMessage.Text = "No such account";
                ErrorMessage.Visible = true;
                return;
            }
            selectedAccountNumber = accountNumber;

        }

        private void loadAccountTable(DataRow info)
        {

        }
        private void loadDataTable(DataRow info)
        {
            foreach(Control control in clientControlIDs)
            {
                string sqlField = getSQLFieldNameByControlID(control.ID);
                if (control is TextBox)
                {
                    TextBox tb = control as TextBox;
                    string content = info[sqlField] as string;
                    if(content != null)
                    {
                        tb.Text = content.Trim();
                    }

                }
            }
        }

        private string getSQLFieldNameByControlID(string id)
        {
            string sqlField = Char.ToLower(id[0]) + id.Substring(1);
            return sqlField;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            string sql = "update [Client] set ";
            int count = clientControlIDs.Count, i = 0;
            // construct sql based on the info given in the table
            foreach(Control control in clientControlIDs)
            {
                i++;
                if(control is TextBox)
                {
                    TextBox tb = control as TextBox;
                    string content = tb.Text.Trim();

                    if (content.Length == 0)
                    {
                        sql += (getSQLFieldNameByControlID(control.ID) + "=NULL" + ((i == count) ? "" : ","));
                    }else
                    {
                        sql += (getSQLFieldNameByControlID(control.ID) + "='" + content + ((i == count) ? "'" : "',"));
                    }

                }
            }

            // finally...
            sql += " where accountNumber='" + accountNumber + "' and charindex('" + firstName + "',firstName) > 0 and charindex('" + lastName + "',lastName) > 0 ";

            // then submit data...
            SqlTransaction t = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);

            InfoPanel.Visible = false;
            ErrorMessage.Text = "Client information has been updated!";
            ErrorMessage.Visible = true;
        }
    }
}