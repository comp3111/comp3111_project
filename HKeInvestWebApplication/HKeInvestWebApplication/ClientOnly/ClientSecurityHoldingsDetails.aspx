﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientSecurityHoldingsDetails.aspx.cs" Inherits="HKeInvestWebApplication.ClientOnly.ClientSecurityHoldingsDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h4>Your Security Holding Details</h4>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-4">
                <asp:Label ID="txtAccountNumber" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="ddlSecurityType" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged">
                    <asp:ListItem Value="0">Security type</asp:ListItem>
                    <asp:ListItem Value="bond">Bond</asp:ListItem>
                    <asp:ListItem Value="stock">Stock</asp:ListItem>
                    <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-4">
                <asp:DropDownList ID="ddlCurrency" runat="server" AutoPostBack="True" Visible="False" CssClass="form-control" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                    <asp:ListItem Value="0">Currency</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div>
            <asp:Label ID="lblClientName" runat="server" Text="" Visible="False" CssClass="col-md-6"></asp:Label>
            <asp:Label ID="lblResultMessage" runat="server" Text="" Visible="False" CssClass="col-md-6"></asp:Label>
        </div>
        <div>
            <asp:GridView ID="gvSecurityHolding" runat="server" Visible="False" AutoGenerateColumns="False" OnSorting="gvSecurityHolding_Sorting" AllowSorting="True" CssClass="table table-bordered">
                <Columns>
                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" SortExpression="code"></asp:BoundField>
                    <asp:BoundField DataField="name" HeaderText="Name" ReadOnly="True" SortExpression="name"></asp:BoundField>
                    <asp:BoundField DataField="shares" DataFormatString="{0:n2}" HeaderText="Shares" ReadOnly="True" SortExpression="shares"></asp:BoundField>
                    <asp:BoundField DataField="base" HeaderText="Base" ReadOnly="True"></asp:BoundField>
                    <asp:BoundField DataField="price" DataFormatString="{0:n2}" HeaderText="Price" ReadOnly="True"></asp:BoundField>
                    <asp:BoundField DataField="value" DataFormatString="{0:n2}" HeaderText="Value" ReadOnly="True" SortExpression="value"></asp:BoundField>
                    <asp:BoundField DataField="convertedValue" DataFormatString="{0:n2}" HeaderText="Value in" ReadOnly="True" SortExpression="convertedValue"></asp:BoundField>
                    <asp:BoundField DataField="profitLoss" DataFormatString="{0:n2}" HeaderText="Value in" ReadOnly="True" SortExpression="convertedValue" Visible="false"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </div>
        <!--TODO: below is the interface for requirement 6a,6c and 6d-->
        <asp:Panel ID="extraPanel" runat="server">
            <div class="form-group">
                <h4> Account Summary </h4>
            </div>
            <div class="form-group">
                <asp:GridView ID="gvSummary" runat="server" Visible="false" CssClass="table table-bordered">
                    <Columns>
                        <asp:BoundField HeaderText="Free Balance (HKD)" ReadOnly="True" DataField="balance"></asp:BoundField>
                        <asp:BoundField HeaderText="Bond Value (HKD)" ReadOnly="True" DataField="bond"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Value (HKD)" ReadOnly="True" DataField="stock"></asp:BoundField>
                        <asp:BoundField HeaderText="Unit Trust Value (HKD)" ReadOnly="True" DataField="unit trust"></asp:BoundField>
                        <asp:BoundField HeaderText="Total Value (HKD)" ReadOnly="True" DataField="total"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <hr /> <!-- active orders-->
            <div class="form-group">
                <h4> Active Orders </h4>
            </div>

            <div class="form-group">
                <asp:Label ID="lblActiveOrders" runat="server" Visible="false" Text="No Active Order placed by this account."></asp:Label>
                <asp:GridView ID="gvActiveOrders" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Date Submitted" ReadOnly="True" DataField="dateSubmitted"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Type" ReadOnly="True" DataField="securityType"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Code" ReadOnly="True" DataField="securityCode"></asp:BoundField>
                        <asp:BoundField HeaderText="Buy/Sell Order" ReadOnly="True" DataField="buyOrSell"></asp:BoundField>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Name" ReadOnly="True" DataField="securityName"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Status" ReadOnly="True" DataField="status"></asp:BoundField>

                        <asp:BoundField HeaderText="Dollar Amount(HKD)" ReadOnly="True" DataField="amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of Shares" ReadOnly="True" DataField="shares"></asp:BoundField>

                        <asp:BoundField HeaderText="Expiry Date" ReadOnly="True" DataField="expiryDay"></asp:BoundField>

                        <asp:BoundField HeaderText="Selling Price(HKD)" ReadOnly="True" DataField="limitPrice"></asp:BoundField>

                        <asp:BoundField HeaderText="Stop Price (HKD)" ReadOnly="True" DataField="stopPrice"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Order Type" ReadOnly="true" DataField="stockOrderType"></asp:BoundField>
                        <asp:BoundField HeaderText="Account Number" ReadOnly="true" DataField="accountNumber" Visible="false"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <hr /> <!--Order history-->
            <div class="form-group">
                <h4> Order history </h4>
            </div>
            <div class="form-group">
                
                <asp:Label AssociateControlID="fromDate" runat="server" Text="Order From " CssClass="control-label col-md-3"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="fromDate" runat="server" CssClass="form-control" OnTextChanged="fromDate_TextChanged"></asp:TextBox>
                    </div>
                <asp:Label AssociateControlID="toDate" runat="server" Text="To" CssClass="control-label col-md-3"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="toDate" runat="server" CssClass="form-control" OnTextChanged="toDate_TextChanged"></asp:TextBox>
                    </div>

            </div>
            <div class="form-group">
                <asp:Label ID="lblOrderHistory" runat="server" Visible="false" Text="No Active Order placed by this account."></asp:Label>
                <asp:GridView ID="gvOrderHistory" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Date Submitted" ReadOnly="True" DataField="dateSubmitted"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Type" ReadOnly="True" DataField="securityType"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Code" ReadOnly="True" DataField="securityCode"></asp:BoundField>
                        <asp:BoundField HeaderText="Buy/Sell Order" ReadOnly="True" DataField="buyOrSell"></asp:BoundField>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Name" ReadOnly="True" DataField="securityName"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Status" ReadOnly="True" DataField="status"></asp:BoundField>

                        <asp:BoundField HeaderText="Dollar Amount(HKD)" ReadOnly="True" DataField="amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of Shares" ReadOnly="True" DataField="shares"></asp:BoundField>

                        <asp:BoundField HeaderText="Expiry Date" ReadOnly="True" DataField="expiryDay"></asp:BoundField>

                        <asp:BoundField HeaderText="Selling Price(HKD)" ReadOnly="True" DataField="limitPrice"></asp:BoundField>

                        <asp:BoundField HeaderText="Stop Price (HKD)" ReadOnly="True" DataField="stopPrice"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Order Type" ReadOnly="true" DataField="stockOrderType"></asp:BoundField>
                        <asp:BoundField HeaderText="Account Number" ReadOnly="true" DataField="accountNumber" Visible="false"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="form-group">
                <asp:Label ID="lblTransaction" runat="server" Visible="false" Text="No Transaction is made for this account"></asp:Label>
                <asp:GridView ID="gvTransaction" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Reference Number" ReadOnly="True" DataField="orderNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Date Executed" ReadOnly="True" DataField="dateExecuted"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of share" ReadOnly="True" DataField="quantity"></asp:BoundField>
                        <asp:BoundField HeaderText="Price per share" ReadOnly="True" DataField="pricePerShare"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:Panel>
        <hr />
    </div>
</asp:Content>

