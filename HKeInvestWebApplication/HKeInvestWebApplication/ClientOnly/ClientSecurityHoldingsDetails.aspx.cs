﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication.ClientOnly
{
    public partial class ClientSecurityHoldingsDetails : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        string username;
        string accountNumber;
        string firstname, lastname;
        // for requirement 6a,6c,6d
        string[] colName = { "Account balance(HKD)", "Bond value (HKD)", "Stock value (HKD)", "unit trust value (HKD)", "Total value(HKD)" };
        string[] colsfield = { "balance", "bond", "stock", "unit trust", "total" };
        protected void Page_Load(object sender, EventArgs e)
        {
            //load currency details
            myHKeInvestCode.getCurrencyInfo(this.Session);
            //get user name and account number first
            //get account number
            username = Context.User.Identity.GetUserName();
            string sqlac = "select accountNumber from Account where userName='" + username + "'";

            DataTable table = myHKeInvestData.getData(sqlac);
            foreach (DataRow row in table.Rows)
            {
                accountNumber = row["accountNumber"].ToString();
            }

            txtAccountNumber.Text = accountNumber;

            //get names
            string sql = "select firstName,lastName from Client where accountNumber='" + accountNumber + "'"; // Complete the SQL statement.

            DataTable dtClient = myHKeInvestData.getData(sql);
            if (dtClient == null) { return; } // If the DataSet is null, a SQL error occurred.

            // Show the client name(s) on the web page.
            string clientName = "Client(s): ";
            int i = 1;
            foreach (DataRow row in dtClient.Rows)
            {
                clientName = clientName + row["lastName"] + ", " + row["firstName"];
                if (dtClient.Rows.Count != i)
                {
                    clientName = clientName + "and ";
                }
                i = i + 1;
            }
            lblClientName.Text = clientName;
            lblClientName.Visible = true;


            if (!IsPostBack)
            {
                // Get the available currencies to populate the DropDownList.
                DataTable dtCurrency = (DataTable)Session["currencyTable"];
                foreach (DataRow row in dtCurrency.Rows)
                {
                    ddlCurrency.Items.Add(row["currency"].ToString().Trim());
                }
            }

            // Requirement 6a, 6c,6d
            loadAccountSummary();
            loadActiveOrders(accountNumber);
            loadOrderHistory(accountNumber);
        }

        protected void ddlSecurityType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset visbility of controls and initialize values.
            lblResultMessage.Visible = false;
            ddlCurrency.Visible = false;
            gvSecurityHolding.Visible = false;
            ddlCurrency.SelectedIndex = 0;
            string sql = "";

            string securityType = ddlSecurityType.Text; // Set the securityType from a web form control!

            // Check if an account number has been specified.
            if (accountNumber == "")
            {
                lblResultMessage.Text = "Please specify an account number.";
                lblResultMessage.Visible = true;
                ddlSecurityType.SelectedIndex = 0;
                return;
            }

            // No action when the first item in the DropDownList is selected.
            if (securityType == "0") { return; }

            // *****************************************************************************************
            // TODO: Construct the SQL statement to retrieve the first and last name of the client(s). *
            // *****************************************************************************************
            // done
            // *****************************************************************************************************************************
            // TODO: Construct the SQL select statement to get the code, name, shares and base of the security holdings of a specific type *
            //       in an account. The select statement should also return three additonal columns -- price, value and convertedValue --  *
            //       whose values are not actually in the database, but are set to the constant 0.00 by the select statement. (HINT: see   *
            //       http://stackoverflow.com/questions/2504163/include-in-select-a-column-that-isnt-actually-in-the-database.)            *   
            // *****************************************************************************************************************************
            sql = "select code,name,shares,base, 0.0 as price, 0.00 as value, 0.00 as convertedValue from SecurityHolding where type='" + securityType + "' and accountNumber='" + accountNumber + "'"; // Complete the SQL statement.

            DataTable dtSecurityHolding = myHKeInvestData.getData(sql);
            if (dtSecurityHolding == null) { return; } // If the DataSet is null, a SQL error occurred.

            // If no result is returned, then display a message that the account does not hold this type of security.
            if (dtSecurityHolding.Rows.Count == 0)
            {
                lblResultMessage.Text = "No " + securityType + "s held in this account.";
                lblResultMessage.Visible = true;
                gvSecurityHolding.Visible = false;
                return;
            }

            // For each security in the result, get its current price from an external system, calculate the total value
            // of the security and change the current price and total value columns of the security in the result.
            int dtRow = 0;
            foreach (DataRow row in dtSecurityHolding.Rows)
            {
                string securityCode = row["code"].ToString();
                decimal shares = Convert.ToDecimal(row["shares"]);
                decimal price = myExternalFunctions.getSecuritiesPrice(securityType, securityCode);
                decimal value = Math.Round(shares * price - (decimal).005, 2);
                dtSecurityHolding.Rows[dtRow]["price"] = price;
                dtSecurityHolding.Rows[dtRow]["value"] = value;
                dtRow = dtRow + 1;
            }

            // Set the initial sort expression and sort direction for sorting the GridView in ViewState.
            ViewState["SortExpression"] = "name";
            ViewState["SortDirection"] = "ASC";

            // Bind the GridView to the DataTable.
            gvSecurityHolding.DataSource = dtSecurityHolding;
            gvSecurityHolding.DataBind();

            // Set the visibility of controls and GridView data.
            gvSecurityHolding.Visible = true;
            ddlCurrency.Visible = true;
            gvSecurityHolding.Columns[myHKeInvestCode.getColumnIndexByName(gvSecurityHolding, "convertedValue")].Visible = false;
        }

        protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the index value of the convertedValue column in the GridView using the helper method "getColumnIndexByName".
            int convertedValueIndex = myHKeInvestCode.getColumnIndexByName(gvSecurityHolding, "convertedValue");

            // Get the currency to convert to from the ddlCurrency dropdownlist.
            // Hide the converted currency column if no currency is selected.
            string toCurrency = ddlCurrency.SelectedValue.ToUpper().Trim();
            if (toCurrency == "0")
            {
                gvSecurityHolding.Columns[convertedValueIndex].Visible = false;
                return;
            }

            // Make the convertedValue column visible and create a DataTable from the GridView.
            // Since a GridView cannot be updated directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            gvSecurityHolding.Columns[convertedValueIndex].Visible = true;
            DataTable dtSecurityHolding = myHKeInvestCode.unloadGridView(gvSecurityHolding);

            // ***********************************************************************************************************
            // TODO: For each row in the DataTable, get the base currency of the security, convert the current value to  *
            //       the selected currency and assign the converted value to the convertedValue column in the DataTable. *
            // ***********************************************************************************************************
            foreach (DataRow row in dtSecurityHolding.Rows)
            {
                // Add your code here!
                string baseCurrency = row["base"].ToString().ToUpper().Trim();
                // store the currency rate if the currency is not null
                decimal baseCurrencyRate = Convert.ToDecimal(Session[baseCurrency]),
                        toCurrencyRate = Convert.ToDecimal(Session[toCurrency]);

                //convert to HKD first
                //       row["convertedValue"] = (Convert.ToSingle(row["value"]) / Convert.ToSingle(Session[toCurrency])).ToString();
                row["convertedValue"] = (Convert.ToSingle(row["value"])) / Convert.ToSingle(toCurrencyRate);
            }

            // Change the header text of the convertedValue column to indicate the currency. 
            gvSecurityHolding.Columns[convertedValueIndex].HeaderText = "Value in " + toCurrency;

            // Bind the DataTable to the GridView.
            gvSecurityHolding.DataSource = dtSecurityHolding;
            gvSecurityHolding.DataBind();
        }

        protected void gvSecurityHolding_Sorting(object sender, GridViewSortEventArgs e)
        {
            // Since a GridView cannot be sorted directly, it is first loaded into a DataTable using the helper method 'unloadGridView'.
            // Create a DataTable from the GridView.
            DataTable dtSecurityHolding = myHKeInvestCode.unloadGridView(gvSecurityHolding);

            // Set the sort expression in ViewState for correct toggling of sort direction,
            // Sort the DataTable and bind it to the GridView.
            string sortExpression = e.SortExpression.ToLower();
            ViewState["SortExpression"] = sortExpression;
            dtSecurityHolding.DefaultView.Sort = sortExpression + " " + myHKeInvestCode.getSortDirection(ViewState, e.SortExpression);
            dtSecurityHolding.AcceptChanges();

            // Bind the DataTable to the GridView.
            gvSecurityHolding.DataSource = dtSecurityHolding.DefaultView;
            gvSecurityHolding.DataBind();
        }

        private void loadAccountSummary()
        {
            extraPanel.Visible = false;
            gvSummary.Visible = false;
            lblResultMessage.Visible = false;
            string accountNumber = txtAccountNumber.Text.Trim();
            // get all securities info by the account number
            string sql = "select code,base,shares,type from [SecurityHolding] where accountNumber='" + accountNumber + "'";
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null) return;
            if (table.Rows.Count == 0)
            {
                // no such user
                lblResultMessage.Text = "No such account.";
                lblResultMessage.Visible = true;
                return;
            }
            // get partial sum of value by the type of securities
            Dictionary<string, decimal> map = new Dictionary<string, decimal>();
            string[] types = { "bond", "unit trust", "stock" };
            // prepare dictionary entries
            foreach (string t in types)
            {
                map.Add(t, 0);
            }
            foreach (DataRow row in table.Rows)
            {
                string type = (row["type"] as string).Trim(),
                       code = (row["code"] as string).Trim(),
                       f_base = (row["base"] as string).Trim();
                decimal shares = Convert.ToDecimal(row["shares"]),
                        val = getValueInHKD(type, code, f_base, shares);
                map[type] += val;
            }

            decimal total = 0;
            // prepare total value
            foreach (decimal value in map.Values)
            {
                total += value;
            }
            if (map.Keys.Count > 0)
            {
                DataTable temp = prepareDataTable();
                //TODO: this doesn't work :(
                DataRow newrow = temp.NewRow();
                // populate row
                newrow[colsfield[0]] = getBalanceOfAccount(accountNumber);
                newrow[colsfield[1]] = map["bond"];
                newrow[colsfield[2]] = map["stock"];
                newrow[colsfield[3]] = map["unit trust"];
                newrow[colsfield[4]] = total;
                temp.Rows.Add(newrow);
                //bind table
                gvSummary.DataSource = temp;
                gvSummary.DataBind();

                gvSummary.Visible = true;
                extraPanel.Visible = true;
            }

        }

        private decimal getValueInHKD(string type, string code, string f_base, decimal shares)
        {
            decimal price = myExternalFunctions.getSecuritiesPrice(type, code),
                cur_rate = myExternalFunctions.getCurrencyRate(f_base);
            return Convert.ToDecimal(shares) * price * cur_rate;
        }
        // get the balance of the account
        private decimal getBalanceOfAccount(string accountNumber)
        {
            string sql = "select balance from [Account] where accountNumber='" + accountNumber + "'";
            DataTable table = myHKeInvestData.getData(sql);
            return Convert.ToDecimal(table.Rows[0]["balance"]);
        }
        private DataTable prepareDataTable()
        {
            DataTable table = new DataTable();
            foreach (string name in colsfield)
            {
                table.Columns.Add(name);
            }
            return table;
        }
        private bool hasUser(string accountNumber)
        {
            string sql = "select * from [Account] where accountNumber='" + accountNumber + "'";
            DataTable t = myHKeInvestData.getData(sql);
            return t.Rows.Count != 0;
        }

        // for requirement 6c
        private void loadActiveOrders(string accountNumber)
        {
            if (accountNumber == null) return;
            // suppress label
            lblActiveOrders.Visible = false;
            DataTable orderTable = myHKeInvestCode.getActiveOrderOfAccount(accountNumber);
            if (orderTable == null)
            {
                lblActiveOrders.Visible = true;
                return;
            }
            gvActiveOrders.DataSource = orderTable;
            gvActiveOrders.DataBind();
            gvActiveOrders.Visible = true;
        }
        private void loadOrderHistory(string accountNumber)
        {
            if (accountNumber == null) return;
            lblOrderHistory.Visible = false;
            gvOrderHistory.Visible = false;

            string from = fromDate.Text.Trim(), to = toDate.Text.Trim();
            DataTable orderTable = null;
            if (from.Length == 0 || to.Length == 0)
            {
                // an incomplete date query, use the normal getOrder method.
                orderTable = myHKeInvestCode.getOrderHistoryOfAccount(accountNumber, null, null);
            }
            else if (!myHKeInvestCode.isDateString(from) || !myHKeInvestCode.isDateString(to))
            {
                // both dates are filled but at least one of them is not in a correct format
                lblOrderHistory.Text = "Date entered in one or more fields is not valid.";
                lblOrderHistory.Visible = true;
                return;

            }
            // input ok, get data now
            orderTable = myHKeInvestCode.getOrderHistoryOfAccount(accountNumber, from, to);
            //TODO: this is not finished. By requirement 6d the table should be filled with transaction record as well but apparently this is not finished at this moment
            if (orderTable == null)
            {
                lblOrderHistory.Text = "No order history is found.";
                lblOrderHistory.Visible = true;
                return;
            }
            gvOrderHistory.DataSource = orderTable;
            gvOrderHistory.DataBind();
            gvOrderHistory.Visible = true;
        }
        private void loadTransactionHistory(string accountNumber)
        {
            if (accountNumber == null) return;
            lblTransaction.Visible = false;
            gvOrderHistory.Visible = false;
            string from = fromDate.Text.Trim(), to = toDate.Text.Trim();
            DataTable orderTable = myHKeInvestCode.getOrderHistoryOfAccount(accountNumber, from, to);
            DataTable results = null;
            foreach (DataRow row in orderTable.Rows)
            {
                string orderNumber = (row["referenceNumber"] as string).Trim();
                string sql = String.Format("select * from [Transaction] where orderNumber='{0}'", orderNumber);
                DataTable table = myHKeInvestData.getData(sql);
                if (table == null || table.Rows.Count == 0) continue;
                if (results == null) results = table;
                else results.Merge(table);
            }
            gvTransaction.DataSource = results;
            gvTransaction.DataBind();
            gvTransaction.Visible = true;

        }
        protected void fromDate_TextChanged(object sender, EventArgs e)
        {
            loadOrderHistory(txtAccountNumber.Text.Trim());
        }

        protected void toDate_TextChanged(object sender, EventArgs e)
        {
            loadOrderHistory(txtAccountNumber.Text.Trim());
        }
    }
}