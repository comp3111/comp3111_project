﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaceOrder.aspx.cs" Inherits="HKeInvestWebApplication.PlaceOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="font-family: 'Times New Roman', Times, serif; font-size: x-large; font-style: normal; font-weight: bolder">PlaceOrder</h2>
    <div class="form-horizontal">
    <div class="form-group">
        <asp:Label ID="ErrorMessage" runat="server" Text="" CssClass="text-danger"></asp:Label>
    </div>
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="account" CssClass="col-md-3" Text="Account :"></asp:Label>
        <asp:Label ID="account" runat="server" CssClass="text-left" Text="account#" Font-Bold="True"></asp:Label>
    </div>
    <div class="form-group">
        <asp:Label ID="Label1" runat="server" AssociatedControlID="balance" CssClass="col-md-3" Text="Balance :"></asp:Label>
        <asp:Label ID="balance" runat="server" CssClass="text-left" Text="$HKD" Font-Bold="True"></asp:Label>
    </div>
    <div class="form-group-sm">
        <asp:Button ID="buy" runat="server" CssClass="btn-success" OnClick="buy_Click" Text="Buy" Height="26px" Width="50px" />
&nbsp;&nbsp;&nbsp;
        <asp:Button ID="sell" runat="server" CssClass="btn-danger" Text="Sell" OnClick="sell_Click" Height="26px" Width="50px" />
        <asp:HiddenField ID="IsBuy" runat="server" Visible="False" />
    </div>
    <div class="form-group">
    </div>
        <asp:Panel ID="panel_default" runat="server" Visible="False">
            <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="security_type" CssClass="col-md-3" Text="Security Type"></asp:Label>
        <asp:DropDownList runat="server" CssClass="col-sm-3" OnSelectedIndexChanged="security_type_SelectedIndexChanged" ID="security_type" AutoPostBack="True">
            <asp:ListItem Selected="True">-- Select --</asp:ListItem>
            <asp:ListItem Value="bond">Bond</asp:ListItem>
            <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
            <asp:ListItem Value="stock">Stock</asp:ListItem>
        </asp:DropDownList>
        <asp:Label runat="server" AssociatedControlID="code" CssClass="col-md-3" Text="Security Code"></asp:Label>
        <asp:TextBox ID="code" runat="server" CssClass="col-sm-3" AutoPostBack="True" TextMode="Number" OnTextChanged="code_TextChanged"></asp:TextBox>
    </div>
    <div class="form-group">
        <asp:Label ID="quantity" runat="server" AssociatedControlID="quantity_num" CssClass="col-md-3" Text="Quantity ($HKD)"></asp:Label>
        <asp:TextBox ID="quantity_num" runat="server" CssClass="col-sm-3" TextMode="Number" AutoPostBack="True" OnTextChanged="quantity_num_TextChanged"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;
        <asp:Label runat="server" AssociatedControlID="amount" CssClass="col-md-3" Text="Current Price"></asp:Label>
        <asp:TextBox ID="amount" runat="server" AutoPostBack="True" CssClass="col-sm-3" ReadOnly="True">Select a Security Type</asp:TextBox>
     </div>
        </asp:Panel>
     <asp:Panel ID="panel_stock" runat="server" Visible="False">
     <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="order_type" CssClass="col-md-3" Text="Order Type"></asp:Label>
        <asp:DropDownList ID="order_type" runat="server" CssClass="col-sm-3" AutoPostBack="True" OnSelectedIndexChanged="order_type_SelectedIndexChanged">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem Value="market">Market Order</asp:ListItem>
            <asp:ListItem Value="limit">Limit Order</asp:ListItem>
            <asp:ListItem Value="stop">Stop Order</asp:ListItem>
            <asp:ListItem Value="stop limit">Stop Limit Order</asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-group">
         <asp:Label ID="limit_label" runat="server" AssociatedControlID="limit_price" CssClass="col-md-3" Text="Limit Price" Visible="False"></asp:Label>
         <asp:TextBox ID="limit_price" runat="server" CssClass="col-sm-3" Visible="False" TextMode="Number" AutoPostBack="True"></asp:TextBox>
         <asp:Label ID="stop_label" runat="server" AssociatedControlID="stop_price" CssClass="col-md-3" Text="Stop Price" Visible="False"></asp:Label>
         <asp:TextBox ID="stop_price" runat="server" CssClass="col-sm-3" Visible="False" TextMode="Number" AutoPostBack="True"></asp:TextBox>
        
        </div>
        <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="expiry_date" CssClass="col-md-3" Text="No. of Days Until Expired"></asp:Label>
        <asp:DropDownList ID="expiry_date" runat="server" CssClass="col-sm-3" AutoPostBack="True" OnSelectedIndexChanged="expiry_date_SelectedIndexChanged">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem>0</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
        </asp:DropDownList>
        <asp:Label runat="server" AssociatedControlID="all_or_none" CssClass="col-md-3" Text="All or None"></asp:Label>
        <asp:DropDownList ID="all_or_none" runat="server" CssClass="col-sm-3" AutoPostBack="True">
            <asp:ListItem>-- Select --</asp:ListItem>
            <asp:ListItem Value="Y">Yes</asp:ListItem>
            <asp:ListItem Value="N">No</asp:ListItem>
        </asp:DropDownList>
        </div>
        <div class ="form-group">

            <asp:Label runat="server" CssClass="col-md-3" Text="Current Date" AssociatedControlID="CurrentDay"></asp:Label>
            <asp:TextBox ID="CurrentDay" runat="server" AutoPostBack="True" CssClass="col-sm-3" ReadOnly="True" TextMode="Date" ></asp:TextBox>
            <asp:Label runat="server" CssClass="col-md-3" Text="Expiry Date" AssociatedControlID="ExpiryDate"></asp:Label>
            <asp:TextBox ID="ExpiryDate" runat="server" AutoPostBack="True" CssClass="col-sm-3" ReadOnly="True" TextMode="Date"></asp:TextBox>

        </div>
            
        </asp:Panel>
        </div>
        <div class="form-group-lg">
              <asp:Button ID="order_btn" runat="server" CssClass="form-group-sm" Text="Place Order" Visible="False" OnClick="order_btn_Click" Width="110px" />
        </div>
    <div>

        <asp:SqlDataSource ID="Order" runat="server" ConnectionString="<%$ ConnectionStrings:HKeInvestConnectionString %>" SelectCommand="SELECT * FROM [Order]"></asp:SqlDataSource>

    </div>
</asp:Content>
