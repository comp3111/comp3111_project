﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using System.Configuration;


namespace HKeInvestWebApplication
{
    public partial class PlaceOrder : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        //define an enum to classify the type of current order
        enum OrderType
        {
            NonOrder,
            BondBuyOrder,
            BondSellOrder,
            UnitTrustBuyOrder,
            UnitTrustSellOrder,
            StockBuyOrder,
            StockSellOrder
        }
        string accountNumber;
        protected void Page_Load(object sender, EventArgs e)
        {
            string username = Context.User.Identity.GetUserName();
            string sql = String.Format("select accountNumber from [Account] where userName='{0}'",username);
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return;
            accountNumber = (table.Rows[0]["accountNumber"] as string ).Trim();

            // update labels
            account.Text = accountNumber;
            balance.Text = Convert.ToString(getBalance());
        }

        protected void buy_Click(object sender, EventArgs e)
        {
            panel_default.Visible = true;
            order_btn.Visible = true;
            IsBuy.Value = "buy";
        }

        protected void sell_Click(object sender, EventArgs e)
        { 
            panel_default.Visible = true;
            order_btn.Visible = true;
            IsBuy.Value = "sell";
        }

        protected void security_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (security_type.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                case 2:
                    panel_stock.Visible = false;
                    quantity.Text = "Quantity ($HKD)";
                    break;
                case 3:
                    panel_stock.Visible = true;
                    quantity.Text = "Quantity (In multiple of 100)";
                    DateTime current = DateTime.Today;
                    CurrentDay.Text = current.ToShortDateString();
                    break;
                default:
                    break;
            }
            CurrentPrice();
        }
        protected void CurrentPrice()
        {
            decimal price = 0;
            if (security_type.SelectedIndex == 0)
            {
                amount.Text = "Select a Security Type";
                return;
            }
            else
                price = myExternalFunctions.getSecuritiesPrice(security_type.SelectedValue, code.Text);
            if (price == -1)
            {
                amount.Text = "Wrong Security Code Input";
                return;
            }
            amount.Text = price.ToString("#.##");
        }
        protected void code_TextChanged(object sender, EventArgs e)
        {
            CurrentPrice();
        }
        protected void quantity_num_TextChanged(object sender, EventArgs e)
        {
            CurrentPrice();
        }
        protected void order_type_SelectedIndexChanged(object sender, EventArgs e)
        {
                switch (order_type.SelectedIndex)
                {
                    case 0:
                    case 1:
                        limit_label.Visible = false;
                        limit_price.Visible = false;
                        stop_label.Visible = false;
                        stop_price.Visible = false;
                        break;
                    case 2:
                        limit_label.Visible = true;
                        limit_price.Visible = true;
                        stop_label.Visible = false;
                        stop_price.Visible = false;
                        break;
                    case 3:
                        limit_label.Visible = false;
                        limit_price.Visible = false;
                        stop_label.Visible = true;
                        stop_price.Visible = true;
                        break;
                    case 4:
                        limit_label.Visible = true;
                        limit_price.Visible = true;
                        stop_label.Visible = true;
                        stop_price.Visible = true;
                        break;
                    default:
                        break;
                }
        }
        protected void order_btn_Click(object sender, EventArgs e)
        {
            ErrorMessage.Visible = false;
            if (security_type.SelectedIndex == 0)
            {
                showErrorMessage("Please specify security type");
                return;
            }
            /*
            string database_HkeInvest = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\Users\Wwc\Source\Repos\comp3111_project\HKeInvestWebApplication\HKeInvestWebApplication\App_Data\HKeInvestDB.mdf; Integrated Security = True";
            string database_External = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = C:\Users\Wwc\Source\Repos\comp3111_project\HKeInvestWebApplication\HKeInvestWebApplication\App_Data\ExternalDatabases.mdf; Integrated Security = True";
            DateTime date = DateTime.Now;
            InsertData(database_HkeInvest, date, 0);
            InsertData(database_External, date, 1);
            */
            decimal amount = Convert.ToDecimal(quantity_num.Text.Trim());
            decimal shares = myExternalFunctions.getSecuritiesPrice(security_type.SelectedValue.Trim(),code.Text.Trim());

            if (!hasEnoughBalance(shares * amount))
            {
                showErrorMessage("Insufficient balance");
                return;
            }
            if(!canSell(code.Text.Trim(),quantity_num.Text.Trim()))
            {
                showErrorMessage("You have insufficient shares to sell");
                return;
            }

            // then insert the entry to order table
            string sql = String.Format("insert into [Order] (buyOrSell,securityType,securityCode,dateSubmitted,status,shares,amount,stockOrderType,expiryDay,allOrNone,limitPrice,stopPrice,securityName,accountNumber)" + 
               " values ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13})",
                     asSQLEntry(IsBuy.Value),
                     asSQLEntry(security_type.SelectedValue.Trim()),
                     asSQLEntry(code.Text.Trim()),
                     asSQLEntry(DateTime.Now.ToString()),
                     asSQLEntry("pending"),
                     asSQLEntry(Convert.ToString(shares)),
                     asSQLEntry(Convert.ToString(amount)),
                     asSQLEntry(order_type.Text.Trim()),
                     asSQLEntry(expiry_date.Text.Trim()),
                     asSQLEntry(all_or_none.Text.Trim()),
                     asSQLEntry(limit_price.Text.Trim()),
                     asSQLEntry(stop_price.Text.Trim()),
                     asSQLEntry(myHKeInvestCode.getSecurityNameByCode(code.Text.Trim())),
                     asSQLEntry(accountNumber)
            );
            SqlTransaction t = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);
            // then submit the order to external system
            // this needs an updated version of the external system
            // and it can not be directly accessed
            //TODO: call one of the methods
            submitOrder();
            sendInvoice();
                chargeUser(classifyIncomingOrder(),security_type.Text.Trim());
            // when recieve transaction, change to amount of shares that the user has.

        }
        private void submitOrder()
        {
            string type = order_type.SelectedValue.Trim();
            string shares = quantity_num.Text.Trim();
            string s_code = code.Text.Trim();
            string expiry = expiry_date.SelectedValue.Trim();
            string aon = (all_or_none.SelectedValue.Trim() == "yes") ? "Y" : "N";
            string limit = limit_price.Text.Trim();
            string stop = stop_price.Text.Trim();

            string referenceNumber = null;
            switch(classifyIncomingOrder())
            {
                case OrderType.StockBuyOrder:
                    referenceNumber = myExternalFunctions.submitStockBuyOrder(s_code, shares, type, expiry, aon, limit, stop);
                    break;
                case OrderType.BondBuyOrder:
                    referenceNumber = myExternalFunctions.submitBondBuyOrder(s_code, shares);
                    break;
                case OrderType.UnitTrustBuyOrder:
                    referenceNumber = myExternalFunctions.submitUnitTrustBuyOrder(s_code, shares);
                    break;
                case OrderType.StockSellOrder:
                    referenceNumber = myExternalFunctions.submitStockSellOrder(s_code, shares, type, expiry, aon, limit, stop);
                    break;
                case OrderType.BondSellOrder:
                    referenceNumber = myExternalFunctions.submitBondSellOrder(s_code, shares);
                    break;
                case OrderType.UnitTrustSellOrder:
                    referenceNumber = myExternalFunctions.submitUnitTrustSellOrder(s_code, shares);
                    break;
                case OrderType.NonOrder:
                    ErrorMessage.Text = "Your input is invalid. Please check it again.";
                    ErrorMessage.Visible = true;
                    return;
            }
        }

        protected void expiry_date_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime expiry = DateTime.Today.AddDays(double.Parse(expiry_date.SelectedValue));
            ExpiryDate.Text = expiry.ToShortDateString();
        }

        private bool hasEnoughBalance(decimal price)
        {
            decimal balance = getBalance();
            return balance >= price;
        }

        private decimal getBalance()
        {
            string sql = String.Format("select balance from [Account] where accountNumber='{0}'", accountNumber);
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return 0.0m;
            return Convert.ToDecimal(table.Rows[0]["balance"]);
        }

        private void showErrorMessage(string msg)
        {
            ErrorMessage.Text = msg;
            ErrorMessage.Visible = true;
        }

        private string asSQLEntry(string input)
        {
            if (input == null || input.Trim().Length == 0) return "NULL";
            return "'" + input.Trim() + "'";
            
        }
        // send invoice to the email account if the order is executed
        private void sendInvoice()
        {
            string sql = String.Format("select title,firstName,lastName,email from [Client] where accountNumber='{0}'", accountNumber);
            DataTable table = myHKeInvestData.getData(sql);
            string email = (table.Rows[0]["email"] as string).Trim();
            string firstName = (table.Rows[0]["firstName"] as string).Trim();
            string lastName = (table.Rows[0]["lastName"] as string).Trim();
            string clientTitle = (table.Rows[0]["title"] as string).Trim();
            string title = "Invoice from HKeInvest";
            string referenceNumber = getLatestReferenceNumber();

            //compose content
            string content = String.Format("Dear {0} {1},\n Thank you for using HKeInvest. The details of your order is as follows: \n Reference Number: {2}\nAccount Number: {3}\nOrder Type:{4}\nSecurity Code:{5}\n Security Name:{6}\n",
                clientTitle,lastName,referenceNumber,accountNumber,order_type.Text.Trim(),code.Text.Trim(),myHKeInvestCode.getSecurityNameByCode(code.Text.Trim()));
            if(classifyIncomingOrder() == OrderType.StockBuyOrder || classifyIncomingOrder() == OrderType.StockSellOrder)
            {
                //TODO: add content with stock order type, date submitted, total number of shares bought, total executed dollar amount of 
                string type = order_type.SelectedValue.Trim();
                string shares = quantity_num.Text.Trim();
                string s_code = code.Text.Trim();
                string expiry = expiry_date.SelectedValue.Trim();
                string aon = (all_or_none.SelectedValue.Trim() == "yes") ? "Y" : "N";
                string limit = limit_price.Text.Trim();
                string stop = stop_price.Text.Trim();
                
                content += String.Format("Stock order type: {0}\n Expiry Day: {1}\n All or None: {2}\n Highest buying Price: {3}\n Stop Price: {4}\n",type,expiry,aon,limit,stop);
            }
//            email = "yttangab@ust.hk";
            // Please, do not send email to a fake address or we will get banned..
            // But if you are fine from being banned, then change the line of code above if necessary .
            myHKeInvestCode.sendEmail(email, title, content);
        }

        private string getLatestReferenceNumber()
        {
            string sql = "select top 1 referenceNumber from [Order] order by referenceNumber desc";
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return null;
            return (table.Rows[0]["referenceNumber"] as string).Trim().PadLeft(8, '0');
        }

        private OrderType classifyIncomingOrder()
        {
            string type = security_type.SelectedValue.Trim();
            if(IsBuy.Value.Trim() == "buy")
            {
                //buy order
                if (type == "stock") return OrderType.StockBuyOrder;
                if (type == "bond") return OrderType.BondBuyOrder;
                if (type == "unit trust") return OrderType.UnitTrustBuyOrder;
                
            }else
            {
                //sell order
                if (type == "stock") return OrderType.StockSellOrder;
                if (type == "bond") return OrderType.BondSellOrder;
                if (type == "unit trust") return OrderType.UnitTrustSellOrder;
            }
            return OrderType.NonOrder;
        }

        private bool canSell(string code,string amount)
        {
            string sql = String.Format("select shares from [SecurityHolding] where accountNumber='{0}' and code='{1}'",accountNumber,code);
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return false;
            decimal have = Convert.ToDecimal(table.Rows[0]["shares"]);
            decimal want = Convert.ToDecimal(amount);
            return have >= want;
        }
        private void chargeUser(OrderType type,string s_type)
        {
            //deduct SERVICE CHARGES from user account
            decimal asset = myHKeInvestCode.getAssetValue(accountNumber);
            decimal temp = 0;
            decimal charge = 0;
            decimal s_min_charge = myHKeInvestCode.getStockMinimumServiceCharge(asset < 1000000);
            switch(type)
            {
                case OrderType.StockBuyOrder:
                    temp = myHKeInvestCode.getStockServiceCharge(s_type.Contains("limit"), s_type.Contains("stop"), asset < 1000000);
                    if (temp < s_min_charge)
                    {
                        charge = s_min_charge;
                    }
                    break;
                case OrderType.StockSellOrder:
                    temp = myHKeInvestCode.getStockServiceCharge(s_type.Contains("limit"), s_type.Contains("stop"), asset < 1000000);
                    if (temp < s_min_charge) charge = s_min_charge;
                    break;
                case OrderType.BondBuyOrder:
                    temp = myHKeInvestCode.getBondUnitTrustServiceCharge(true, asset < 500000);
                    break;
                case OrderType.BondSellOrder:
                    temp = myHKeInvestCode.getBondUnitTrustServiceCharge(false,asset < 500000);
                    break;
                case OrderType.UnitTrustBuyOrder:
                    temp = myHKeInvestCode.getBondUnitTrustServiceCharge(true, asset < 500000);
                    break;
                case OrderType.UnitTrustSellOrder:
                    temp = myHKeInvestCode.getBondUnitTrustServiceCharge(false, asset < 500000);
                    break;
                default:
                    return;
            }
            if(temp >0 && temp < 1)
            {
                charge = asset * temp;
            }
            string sql = "select balance from [Account] where accountNumber='" + accountNumber + "'";
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return;
            decimal balance = Convert.ToDecimal(table.Rows[0]["balance"]);
            decimal result = balance - charge;

            // transact
            SqlTransaction t = myHKeInvestData.beginTransaction();
            sql = String.Format("update Account set balance='{0}'",result);
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);
        }
    }
}