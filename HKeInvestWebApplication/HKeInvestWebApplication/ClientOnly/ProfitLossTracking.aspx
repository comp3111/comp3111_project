﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProfitLossTracking.aspx.cs" Inherits="HKeInvestWebApplication.ClientOnly.ProfitLossTracking" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 style="font-family: 'Times New Roman', Times, serif; font-size: x-large; font-style: normal; font-weight: bolder">Profit/Loss Tracking</h2>
    <div>
        <asp:Label runat="server" Text="Display" AssociatedControlID="display"></asp:Label>
        <asp:DropDownList ID="display" runat="server" AutoPostBack="True" OnSelectedIndexChanged="display_SelectedIndexChanged">
            <asp:ListItem Value="-- Select a way --"></asp:ListItem>
            <asp:ListItem>All Security</asp:ListItem>
            <asp:ListItem>By Type</asp:ListItem>
            <asp:ListItem>By Indivdual</asp:ListItem>
        </asp:DropDownList>         
    </div>
    <div>

        <asp:Panel ID="panelType" runat="server" Visible="False">
            <asp:Label runat="server" Text="Sort By" AssociatedControlID="sortByType"></asp:Label>
            <asp:DropDownList ID="sortByType" runat="server" AutoPostBack="True">
                <asp:ListItem Value="-- Select a Type --"></asp:ListItem>
                <asp:ListItem Value="bond">Bond</asp:ListItem>
                <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                <asp:ListItem Value="stock">Stock</asp:ListItem>
            </asp:DropDownList>       
        </asp:Panel>

    </div>
    <div>

        <asp:Panel ID="panelIndividual" runat="server" Visible="False">
            <asp:Label runat="server" Text="Select Type" AssociatedControlID="selectType"></asp:Label>
            <asp:DropDownList ID="selectType" runat="server" AutoPostBack="True">
                <asp:ListItem>-- Select Type --</asp:ListItem>
                <asp:ListItem Value="bond">Bond</asp:ListItem>
                <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
                <asp:ListItem Value="stock">Stock</asp:ListItem>
            </asp:DropDownList>
            <asp:Label runat="server" AssociatedControlID="selectCode" Text="Select Code"></asp:Label>
            <asp:DropDownList ID="selectCode" runat="server" AutoPostBack="True" DataSourceID="SqlOrderTable" DataTextField="securityCode" DataValueField="securityCode">
            </asp:DropDownList>
        </asp:Panel>

    </div>
    <div>

        <asp:Button ID="check" runat="server" Text="Check" OnClick="check_Click" />

    </div>
    <div>
            <asp:Label runat="server" Text="Security Type :" AssociatedControlID="securityType" ID="securityTypeLabel" Visible="False"></asp:Label>
            <asp:Label ID="securityType" runat="server" Text="Type" Visible="False"></asp:Label>
            <br />
        <asp:Panel ID="panelView" runat="server" Visible="False">
            <asp:Label runat="server" AssociatedControlID="securityCode" Text="Security Code :"></asp:Label>
            <asp:Label ID="securityCode" runat="server" Text="Code"></asp:Label>
            <br />
            <asp:Label runat="server" AssociatedControlID="securityName" Text="Security Name :"></asp:Label>
            <asp:Label ID="securityName" runat="server" Text="Name"></asp:Label>
            <br />
            <asp:Label runat="server" AssociatedControlID="shareHeld" Text="Shares Held :"></asp:Label>
            <asp:Label ID="shareHeld" runat="server" Text="####"></asp:Label>

        </asp:Panel>

    </div>
    <div>
        <asp:Panel ID="panelProfit" runat="server" Visible="False">
            <asp:Label runat="server" Text="Total Dollar Amount for Buying :" AssociatedControlID="dollarBuy"></asp:Label>
            <asp:Label ID="dollarBuy" runat="server" Text="$$$$"></asp:Label>
            <br />
            <asp:Label runat="server" AssociatedControlID="dollarSell" Text="Total Dollar Amount for Selling :"></asp:Label>
            <asp:Label ID="dollarSell" runat="server" Text="$$$$"></asp:Label>
            <br />
            <asp:Label runat="server" AssociatedControlID="feePaid" Text="Total Fees Paid :"></asp:Label>
            <asp:Label ID="feePaid" runat="server" Text="$$$$"></asp:Label>
            <br />
            <asp:Label runat="server" AssociatedControlID="profitLoss" Text="Profit/Loss :"></asp:Label>
            <asp:Label ID="profitLoss" runat="server" Text="$$$$"></asp:Label>
            <asp:Label ID="Percentage" runat="server" Text="%%"></asp:Label>
        </asp:Panel>

    </div>
    <div>

        <asp:HiddenField ID="accountNum" runat="server" Visible="False" Value="HU00000001" />
        <asp:HiddenField ID="accountBalance" runat="server" Visible="False" Value="0" />

        <asp:SqlDataSource ID="SqlOrderTable" runat="server" ConnectionString="<%$ ConnectionStrings:HKeInvestConnectionString %>" SelectCommand="SELECT [securityCode] FROM [Order] WHERE (([accountNumber] = @accountNumber) AND ([securityType] = @securityType))">
            <SelectParameters>
                <asp:ControlParameter ControlID="accountNum" DefaultValue="" Name="accountNumber" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="selectType" Name="securityType" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
</asp:Content>
