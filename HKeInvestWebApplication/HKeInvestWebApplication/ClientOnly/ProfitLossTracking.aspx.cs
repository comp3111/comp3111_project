﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;
using System.Data.SqlClient;
using System.Configuration;

namespace HKeInvestWebApplication.ClientOnly
{
    public partial class ProfitLossTracking : System.Web.UI.Page
    {
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        
        decimal totalBuyAmount = 0;
        decimal totalSellAmount = 0;
        decimal fee = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string username = Context.User.Identity.GetUserName();
            string sql = "select accountNumber,balance from [Account] where userName='" + username + "'";
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0) return;
            accountNum.Value = (table.Rows[0]["accountNumber"] as string).Trim();
            accountBalance.Value = (Convert.ToDecimal(table.Rows[0]["balance"])).ToString();
        }

        protected void display_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (display.SelectedIndex)
            {
                case 0:
                    panelType.Visible = false;
                    panelIndividual.Visible = false;
                    break;
                case 1:
                    panelType.Visible = false;
                    panelIndividual.Visible = false;
                    break;
                case 2:
                    panelType.Visible = true;
                    panelIndividual.Visible = false;
                    break;
                case 3:
                    panelType.Visible = false;
                    panelIndividual.Visible = true;
                    break;
            }

        }
        protected void check_Click(object sender, EventArgs e)
        {
            decimal balance;
            decimal percent;
            panelIndividual.Visible = false;
            panelProfit.Visible = false;
            securityTypeLabel.Visible = false;
            securityType.Visible = false;
            switch (display.SelectedIndex)
            {
                case 0:
                    return;
                case 1:
                    getStockProfit();
                    getBondUnitTrustProfit("bond");
                    getBondUnitTrustProfit("unit trust");
                    break;
                case 2:
                    switch (sortByType.SelectedIndex)
                    {
                        case 0:
                            return;
                        case 1:
                            getBondUnitTrustProfit("bond");
                            break;
                        case 2:
                            getBondUnitTrustProfit("unit trust");
                            break;
                        case 3:
                            getStockProfit();
                            break;
                    }
                    securityType.Text = sortByType.SelectedValue;
                    securityTypeLabel.Visible = true;
                    securityType.Visible = true;
                    break;
                case 3:
                    getIndividualSecurity();
                    securityCode.Text = selectCode.SelectedValue;
                    securityType.Text = selectType.SelectedValue;
                    securityTypeLabel.Visible = true;
                    securityType.Visible = true;
                    panelIndividual.Visible = true;
                    break;
            }
            dollarBuy.Text = totalBuyAmount.ToString();
            dollarSell.Text = totalSellAmount.ToString();
            feePaid.Text = fee.ToString();
            balance = Convert.ToDecimal(accountBalance.Value);
            percent = ((totalSellAmount - totalBuyAmount - fee) / balance) * 100;
            if (percent >= 0)
            {
                Percentage.Text = "(+" + percent.ToString("#.##") + "%)";
                profitLoss.Text = "+" + (totalSellAmount - totalBuyAmount - fee).ToString();
            }
            panelProfit.Visible = true;
        }
        
        protected void getStockProfit() 
        {
            string sql, referenceNumber, stockOrderType, accountNumber;
            decimal price, shares, balance;
            accountNumber = accountNum.Value;
            balance = Convert.ToDecimal(accountBalance.Value);
            sql = "select referenceNumber,buyOrSell,stockOrderType from [Order] where accountNumber = '" + accountNumber + "' and securityType = 'stock'";
            DataTable orderTable = myHKeInvestData.getData(sql);
            if (orderTable == null || orderTable.Rows.Count == 0) return;
            foreach (DataRow row in orderTable.Rows)
            {
                referenceNumber = (row["referenceNumber"]as string).Trim();
                DataTable transaction = myExternalFunctions.getOrderTransaction(referenceNumber);
                if (transaction == null)
                    continue;
                stockOrderType = (row["stockOrderType"]as string).Trim();
                price = Convert.ToDecimal(transaction.Rows[0]["executePrice"]);
                shares = Convert.ToDecimal(transaction.Rows[0]["executeShares"]);
                fee += stockOrderFee(price * shares, stockOrderType, balance);
                if ((row["buyOrSell"]as string).Trim() == "buy")
                    totalBuyAmount += price * shares;
                else if ((row["buyOrSell"] as string).Trim() == "sell")
                    totalSellAmount += price * shares;
            }
        }
        protected void getBondUnitTrustProfit(string securityType)
        {
            string sql, accountNumber, buyOrSell;
            decimal amount, balance;
            accountNumber = accountNum.Value;
            balance = Convert.ToDecimal(accountBalance.Value);
            sql = "select buyOrSell,amount from [Order] where accountNumber = '" + accountNumber + "' and securityType = '"+ securityType +"'";
            DataTable orderTable = myHKeInvestData.getData(sql);
            if (orderTable == null || orderTable.Rows.Count == 0) return;
            foreach (DataRow row in orderTable.Rows)
            {
                amount = Convert.ToDecimal(row["amount"]);
                buyOrSell = (row["buyOrSell"]as string).Trim();
                fee += bondUnitTrustOrderFee(amount, buyOrSell, balance);
                if ((row["buyOrSell"] as string).Trim() == "buy")
                    totalBuyAmount += amount;
                else if ((row["buyOrSell"] as string).Trim() == "sell")
                    totalSellAmount += amount;
            }
        }
        protected decimal bondUnitTrustOrderFee(decimal amount, string buyOrSell, decimal balance)
        {
            decimal transactionFee = 0;
            if (balance < 500000)
            {
                if (buyOrSell == "buy")
                    transactionFee = amount * (decimal)0.05;
                if (buyOrSell == "sell")
                    transactionFee = 100;
            }
            else
            {
                if (buyOrSell == "buy")
                    transactionFee = amount * (decimal)0.03;
                if (buyOrSell == "sell")
                    transactionFee = 50;
            }
            return transactionFee;
        }
        protected decimal stockOrderFee(decimal amount,string orderType,decimal balance)
        {
            decimal transactionFee = 0;
            if (balance < 1000000)
            {
                if (orderType == "market")
                    transactionFee = amount * (decimal)0.004;
                else if (orderType == "limit" || orderType == "stop")
                    transactionFee = amount * (decimal)0.006;
                else if (orderType == "stop limit")
                    transactionFee = amount * (decimal)0.008;
                if (transactionFee > 150)
                    return transactionFee;
                else
                    return 150;
            }
            else
            {
                if (orderType == "market")
                    transactionFee = amount * (decimal)0.002;
                else if (orderType == "limit" || orderType == "stop")
                    transactionFee = amount * (decimal)0.004;
                else if (orderType == "stop limit")
                    transactionFee = amount * (decimal)0.006;
                if (fee > 100)
                    return transactionFee;
                else
                    return 100;
            }
        }
        protected void getIndividualSecurity()
        {

        }


    }
}