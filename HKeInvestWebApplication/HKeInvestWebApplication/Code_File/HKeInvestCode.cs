﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Data;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace HKeInvestWebApplication.Code_File
{
    //**********************************************************
    //*  THE CODE IN THIS CLASS CAN BE MODIFIED AND ADDED TO.  *
    //**********************************************************
    public class HKeInvestCode
    {
        public decimal getAssetValue(string accountNumber)
        {
            decimal asset = 0;
            HKeInvestData data = new HKeInvestData();
            ExternalFunctions external = new ExternalFunctions();
            string sql = String.Format("select shares,base from SecurityHoldings where accountNumber = '{0}'",accountNumber);
            DataTable table = data.getData(sql);
            if (table != null && table.Rows.Count == 0)
            {
                foreach(DataRow row in table.Rows)
                {
                    string s_base = (row["base"] as string).Trim();
                    decimal share = Convert.ToDecimal(row["shares"]);
                    decimal rate = external.getCurrencyRate(s_base);
                    asset += share * rate;
                }
            }
            sql = String.Format("select balance from Account where accountNumber = '{0}'", accountNumber);
            table = data.getData(sql);
            if (table != null && table.Rows.Count == 0)
            {
                asset += Convert.ToDecimal(table.Rows[0]["balance"]);

            }
            return asset;
        }
        public string getSecurityNameByCode(string code)
        {
            ExternalFunctions external = new ExternalFunctions();
            string[] types = { "stock", "bond", "unit trust" };
            foreach (string type in types)
            {
                DataTable temp = external.getSecuritiesData(type);
                foreach (DataRow row in temp.Rows)
                {
                    if ((row["code"] as string).Trim() == code) return (row["name"] as string).Trim();
                }
            }
            return null;
        }
        public string getSecurityTypeByCode(string code)
        {
            ExternalFunctions external = new ExternalFunctions();
            string[] types = { "stock", "bond", "unit trust" };
            foreach(string type in types)
            {
                DataTable temp = external.getSecuritiesData(type);
                foreach(DataRow row in temp.Rows)
                {
                    if ((row["code"] as string).Trim() == code) return type;
                }
            }
            return null;
        }
        public DataTable getUserInfo(string sql)
        {
            SqlConnection HKeInvestDBConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            try
            {
                if (sql.Trim() == "")
                {
                    throw new ArgumentException("The SQL statement is empty.");
                }

                DataTable dt = new DataTable();
                if (HKeInvestDBConnection.State != ConnectionState.Open)
                {
                    HKeInvestDBConnection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(sql, HKeInvestDBConnection);
                    da.Fill(dt);
                    HKeInvestDBConnection.Close();
                }
                else
                {
                    SqlDataAdapter da = new SqlDataAdapter(sql, HKeInvestDBConnection);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }

            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }

        public void sendEmail(string email, string title, string content)
        {
            SmtpClient emailServer = new SmtpClient("smtp.cse.ust.hk");

            MailMessage mail = new MailMessage();
            mail.To.Add(email);
            mail.From = new MailAddress("comp3111_team123@cse.ust.hk","Team 123");
            mail.Subject = title;
            mail.Body = content;

            emailServer.Send(mail);
        }
        public bool isDateString(string input)
        {
            string[] ddmmyy = input.Split('/');
            if (ddmmyy.Length != 3) return false;
            int date = 0;
            int month = 0;
            int year = 0;
            int[] big_month = { 1, 3, 5, 7, 8, 10, 12 };

            for (int i = 0; i < ddmmyy.Length; i++)
            {
                if(ddmmyy[i].Length == 0)
                {
                    return false;
                }
                int temp;
                if(!Int32.TryParse(ddmmyy[i],out temp))
                {
                    return false;
                }
                if (i == 0) date = temp;
                if (i == 1) month = temp;
                if (i == 2) year = temp;

            }
            if (big_month.Contains(month))
            {
                return 0 < date && date <= 31;
            }else if(month == 2)
            {
                // check lunar year
                int last = (year % 4 == 0) ? 29 : 28;
                return 0 < date && date <= last;
            }else
            {
                return 0 < date && date <= 30;
            }

        }
	// requirement 6d
        public DataTable getOrderHistoryOfAccount(string accountNumber,string fd,string td)
        {
            // no date selection
            string sql = "select * from [Order] where accountNumber='" + accountNumber + "' and status='complete'";
            // with date selection
            if (fd != null && td != null)
            {
                sql = "select * from [Order] where accountNumber='" + accountNumber + "' and status='complete' and dateSubmitted BETWEEN '" + fd + "' and '" + td + "'";
            }
            HKeInvestData data = new HKeInvestData();
            DataTable table = data.getData(sql);
            if (table == null || table.Rows.Count == 0) return null;
            return table;
        }


        public DataTable getOrdersOfAccount(string accountNumber)
        {
            string sql = "select * from [Order] where accountNumber='" + accountNumber + "'";
            HKeInvestData data = new HKeInvestData();
            DataTable table = data.getData(sql);
            if (table == null || table.Rows.Count == 0) return null;
            return table;
        }

        public DataTable getActiveOrderOfAccount(string accountNumber)
        {
            string sql = "select * from [Order] where accountNumber='" + accountNumber + "' and (status='" + "pending" +"' or status='partial')";
            HKeInvestData data = new HKeInvestData();
            DataTable table = data.getData(sql);
            if (table == null || table.Rows.Count == 0) return null;
            return table;
        }
        public void getCurrencyInfo(System.Web.SessionState.HttpSessionState session)
        {

            ExternalFunctions sys = new ExternalFunctions();
            DataTable dtCurrency = sys.getCurrencyData();
            string[] cur_values = { "EUR", "GBP", "HKD", "JPY", "USD" };
            foreach (DataRow row in dtCurrency.Rows)
            {
                session[row["currency"].ToString().Trim()] = row["rate"].ToString().Trim();
            }
            session["currencyTable"] = dtCurrency;
        }

        public string getDataType(string value)
        {
            // Returns the data type of value. Tests for more types can be added if needed.
            if (value != null)
            {
                int n; decimal d; DateTime dt;
                if (int.TryParse(value, out n)) { return "System.Int32"; }
                else if (decimal.TryParse(value, out d)) { return "System.Decimal"; }
                else if (DateTime.TryParse(value, out dt)) { return "System.DataTime"; }
            }
            return "System.String";
        }

        public string getSortDirection(System.Web.UI.StateBag viewState, string sortExpression)
        {
            // If the GridView is sorted for the first time or sorting is being done on a new column, 
            // then set the sort direction to "ASC" in ViewState.
            if (viewState["SortDirection"] == null || viewState["SortExpression"].ToString() != sortExpression)
            {
                viewState["SortDirection"] = "ASC";
            }
            // Othewise if the same column is clicked for sorting more than once, then toggle its SortDirection.
            else if (viewState["SortDirection"].ToString() == "ASC")
            {
                viewState["SortDirection"] = "DESC";
            }
            else if (viewState["SortDirection"].ToString() == "DESC")
            {
                viewState["SortDirection"] = "ASC";
            }
            return viewState["SortDirection"].ToString();
        }

        public DataTable unloadGridView(GridView gv)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < gv.Columns.Count; i++)
            {
                dt.Columns.Add(((BoundField)gv.Columns[i]).DataField);
            }

            // For correct sorting, set the data type of each DataTable column based on the values in the GridView.
            gv.SelectedIndex = 0;
            for (int i = 0; i < gv.Columns.Count; i++)
            {
                dt.Columns[i].DataType = Type.GetType(getDataType(gv.SelectedRow.Cells[i].Text));
            }

            // Load the GridView data into the DataTable.
            foreach (GridViewRow row in gv.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gv.Columns.Count; j++)
                {
                    dr[((BoundField)gv.Columns[j]).DataField.ToString().Trim()] = row.Cells[j].Text;
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
        //for requirement 5a
        public decimal getBondUnitTrustServiceCharge(bool is_buying,bool asset_less_than)
        {
            if(is_buying)
            {
                if (asset_less_than) return 0.05m;
                else return 100;
            }else
            {
                if (asset_less_than) return 0.03m;
                else return 50;
            }
        }
        public decimal getStockMinimumServiceCharge(bool asset_less_than)
        {
            if (asset_less_than) return 150;
            return 100;
        }
        public decimal getStockServiceCharge(bool is_limit,bool is_stop,bool asset_less_than)
        {
            if(is_limit && is_stop)
            {
                if (asset_less_than) return 0.008m;
                else return 0.006m;
            }
            if(is_limit || is_stop)
            {
                if (asset_less_than) return 0.006m;
                else return 0.004m;
            }
            if (asset_less_than) return 0.004m;
            else return 0.002m;
        }
        public int getColumnIndexByName(GridView gv, string columnName)
        {
            // Helper method to get GridView column index by a column's DataField name.
            for (int i = 0; i < gv.Columns.Count; i++)
            {
                if (((BoundField)gv.Columns[i]).DataField.ToString().Trim() == columnName.Trim())
                { return i; }
            }
            MessageBox.Show("Column '" + columnName + "' was not found \n in the GridView '" + gv.ID.ToString() + "'.");
            return -1;
        }

        public decimal convertCurrency(string fromCurrency, string fromCurrencyRate, string toCurrency, string toCurrencyRate, decimal value)
        {
            if(fromCurrency == toCurrency)
            {
                return value;
            }
            else
            {
                return Math.Round(Convert.ToDecimal(fromCurrencyRate) / Convert.ToDecimal(toCurrencyRate) * value - (decimal).005, 2);
            }
        }
        public DataTable getActiveAlertOfAccount (string accountNumber)
        {
            string sql = "select * from [Alert] where accountNumber='" + accountNumber + "'";
            HKeInvestData data = new HKeInvestData();
            DataTable table = data.getData(sql);
            if (table == null || table.Rows.Count == 0) return null;
            return table;
        }
    }
}
