﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using System.Threading;
using System.Data;
namespace HKeInvestWebApplication.Code_File
{
    // singleton, one instance is allowed..
    public class SecuritiesPortfolioManager
    {
        private static Thread thread;
        private static SecuritiesPortfolioManager instance = null;
        private static ExternalFunctions socket;
        private static HKeInvestData data;
        private static List<string> orderReferences;
        public static SecuritiesPortfolioManager getSecuritesPortfolioManager()
        {
                if (instance == null)
                {
                     instance = new SecuritiesPortfolioManager();
                }
                return instance;
        }
        private SecuritiesPortfolioManager()
        {
            socket = new ExternalFunctions();
            thread = new Thread(Task);
            thread.IsBackground = true;
            // load order references from HKeInvestData
            data = new HKeInvestData();

        }

        private void loadOrderReferences()
        {
            if (orderReferences == null) orderReferences = new List<string>();
            orderReferences.Clear();

            DataTable table = data.getData("select referenceNumber from [Order]");
            foreach(DataRow row in table.Rows)
            {
                orderReferences.Add((row["referenceNumber"] as string).Trim());
            }
        }
        private List<String> getTransactionReference()
        {
            List<String> res = new List<String>();
            DataTable rrr = null;
            foreach(string orderRef in orderReferences)
            {
                string sql = String.Format("select referenceNumber from [Transaction] where orderNumber = '{0}'", orderRef);
                DataTable table = data.getData(sql);
                if(table != null && table.Rows.Count != 0)
                {
                    if (rrr != null) rrr.Merge(table);
                    else rrr = table;
                }
            }
            foreach(DataRow r in rrr.Rows)
            {
                res.Add((r["referenceNumber"] as string).Trim());
            }
            return res;
        }
        private List<String> getNewTransactionNumbers()
        {
            List<String> oldRef = getTransactionReference();
            List<String> res = new List<String>();
            // look through all orders
            foreach(string referenceNumber in oldRef)
            {
                // find out pending or partial orders
                string sql = "select * from [Transaction] where referenceNumber='" + referenceNumber + "'";
                DataTable table = data.getData(sql);
                if(table == null || table.Rows.Count == 0)
                {
                    res.Add(referenceNumber);
                }
            }
            return res;
        }
        private void processTransactions()
        {
            foreach(string reference in getNewTransactionNumbers())
            {

            }
        }
        private void Task()
        {
            do
            {
                // TODO: keep querying external database here
                processTransactions();
                Thread.Sleep(100);
            } while (true);
        }

        // for each new transaction, send email to the corresponding user



        public void run()
        {
            thread.Start();
        }
    }
}