﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientInformation.aspx.cs" Inherits="HKeInvestWebApplication.EmployeeOnly.ClientInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Create a new login account</h2>

    <div class="form-group">
         <h4>Client Information</h4>
    </div>
    <div class="form-horizontal">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" EnableClientScript="False" />

        <asp:Literal ID="Error" runat="server" Visible="false"></asp:Literal>
        <div class="form-group">
            <asp:Label AssociatedControlID="title" runat="server" Text="Title"></asp:Label>
            <asp:DropDownList ID="title" runat="server">

                <asp:ListItem>Mr.</asp:ListItem>
                <asp:ListItem>Miss</asp:ListItem>
                <asp:ListItem>Mrs.</asp:ListItem>
                <asp:ListItem>Dr.</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="ddlAccountType" runat="server" Text="Account Type" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4">
                <asp:CustomValidator ID="CustomValidator1" runat="server" OnServerValidate="AccountType_ServerValidate" ErrorMessage="Please select account type" ControlToValidate="ddlAccountType" Display="Dynamic" EnableClientScript="false" CssClass="text-danger">*</asp:CustomValidator>
                <asp:DropDownList ID="ddlAccountType" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">Account Type</asp:ListItem>
                    <asp:ListItem Value="individual">Individual</asp:ListItem>
                    <asp:ListItem Value="survivor">Survivorship</asp:ListItem>
                    <asp:ListItem Value="common">Common</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

    <div class="form-group">

        <asp:Label runat="server" Text="First Name" AssociatedControlID="FirstName" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="FirstName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="First Name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>

        <asp:Label runat="server" Text="Last Name" AssociatedControlID="LastName" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="LastName" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Last Name is required." Display="Dynamic">*</asp:RequiredFieldValidator>
            </div>

    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Account #" AssociatedControlID="AccountNumber" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="AccountNumber" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
            <asp:CustomValidator ID="cvAccountNumber" runat="server" ControlToValidate="AccountNumber" CssClass="text-danger" EnableClientScript="False" ErrorMessage="The account number does not match the client's last name." OnServerValidate="cvAccountNumber_ServerValidate" ValidateEmptyText="True" Display="Dynamic">*</asp:CustomValidator>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="AccountNumber" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Account number is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="HKID/Passport#" AssociatedControlID="HKID" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="HKID" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="HKID_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="HKID" CssClass="text-danger" EnableClientScript="False" ErrorMessage="HKID/Passport number is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form-group">
        <asp:CustomValidator ID="cvPassport" ControlToValidate="HKID" runat="server" OnServerValidate="cvPassport_ServerValidate" ErrorMessage="If HKID is not provided, both passport country of issue and passport number should be provided"></asp:CustomValidator>
        <asp:Label runat="server" Text="Passport country of issue" AssociatedControlID="passport_country_of_issue" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="passport_country_of_issue" runat="server" CssClass="form-control" MaxLength="70">

        </asp:TextBox>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Date of Birth" AssociatedControlID="DateOfBirth" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="DateOfBirth" runat="server" CssClass="form-control"></asp:TextBox>
             <asp:RegularExpressionValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Date of Birth is not valid." ForeColor="Red" ValidationExpression="^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}" Display="Dynamic">*</asp:RegularExpressionValidator>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="DateOfBirth" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Date of birth is required." Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

        <asp:Label runat="server" Text="Email" AssociatedControlID="Email" CssClass="control-label col-md-2"></asp:Label>
         <div class="col-md-4"><asp:TextBox ID="Email" runat="server" CssClass="form-control" MaxLength="25" TextMode="Email"></asp:TextBox>
             <asp:RequiredFieldValidator runat="server" ControlToValidate="Email" CssClass="text-danger" EnableClientScript="False" ErrorMessage="Email address is required" Display="Dynamic">*</asp:RequiredFieldValidator>
        </div>

    </div>

    <div class="form-group">

        <asp:Label runat="server" AssociatedControlID="Building" Text="Building" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="Building"  runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
        </div>

        <asp:Label runat="server" AssociatedControlID="Street" Text="Street" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="Street" runat="server"  CssClass="form-control" MaxLength="35"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <asp:Label runat="server" AssociatedControlID="District" Text="District" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="District" runat="server"  CssClass="form-control" MaxLength="19"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">
        <asp:CustomValidator ID="phoneValidator" CssClass="text-danger" runat="server" OnServerValidate="phoneValidator_ServerValidate" ErrorMessage="CustomValidator" ControlToValidate="home_phone" EnableClientScript="false" Display="Dynamic" ValidateEmptyText="true">*</asp:CustomValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid phone number" ControlToValidate="home_phone" ValidationExpression="^[0-9]{8}$" Display="Dynamic" EnableClientScript="false" Text="*"></asp:RegularExpressionValidator>
        <asp:Label runat="server" AssociatedControlID="home_phone" Text="Home Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="home_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>
        
        <asp:Label runat="server" AssociatedControlID="home_fax" Text="Home Fax" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="home_fax" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>


    </div>

    <div class="form-group">
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Invalid phone number" ControlToValidate="home_phone" ValidationExpression="^[0-9]{8}$" Display="Dynamic" EnableClientScript="false" Text="*"></asp:RegularExpressionValidator>
        <asp:Label runat="server" AssociatedControlID="business_phone" Text="Business Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="business_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid phone number" ControlToValidate="home_phone" ValidationExpression="^[0-9]{8}$" Display="Dynamic" EnableClientScript="false" Text="*"></asp:RegularExpressionValidator>
        <asp:Label runat="server" AssociatedControlID="mobile_phone" Text="Mobile Phone" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="mobile_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="text-danger" ControlToValidate="country_of_citizenship" EnableClientScript="false" runat="server" ErrorMessage="Country of Citizenship is required" Display="Dynamic">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="country_of_citizenship" Text="Country of citizenship" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="country_of_citizenship" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="text-danger" EnableClientScript="false" runat="server" ErrorMessage="Country of legal residence is required" Display="Dynamic" ControlToValidate="country_of_residence">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="country_of_residence" Text="Country of legal residence" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4"><asp:TextBox ID="country_of_residence" runat="server" CssClass="form-control" MaxLength="70"></asp:TextBox>
        </div>

    </div>
    <hr />
    <div class="form-group">
            <h4>Employment Information</h4>
    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Employment Status" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" CssClass="form-control" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                <asp:ListItem Value="0">Employment Status</asp:ListItem>
                <asp:ListItem Value="employed">Employed</asp:ListItem>
                <asp:ListItem Value="self_employed">Self-employed</asp:ListItem>
                <asp:ListItem Value="retired">Retired</asp:ListItem>
                <asp:ListItem Value="student">Student</asp:ListItem>
                <asp:ListItem Value="not_employed">Not Employed</asp:ListItem>
                <asp:ListItem Value="homemaker">Homemaker</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">
        <asp:RequiredFieldValidator ControlToValidate="specific_occupation" EnableClientScript="false" Display="Dynamic" CssClass="text-danger" ID="cvSpecificOccupation" runat="server" ErrorMessage="Specific occupation required" Enabled="false">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="specific_occupation" Text="Specific occupation" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="specific_occupation" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator ControlToValidate="years" EnableClientScript="false" Display="Dynamic" CssClass="text-danger" ID="cvYears" runat="server" ErrorMessage="Years with employer required" Enabled="false">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="years" Text="Years with employer" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="years" runat="server" CssClass="form-control" MaxLength="2"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">
        <asp:RequiredFieldValidator ControlToValidate="employer_name" EnableClientScript="false" Display="Dynamic" CssClass="text-danger" ID="cvEmployerName" runat="server" ErrorMessage="Employer Name required" Enabled="false">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="employer_name" Text="Employer name" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="employer_name" runat="server" CssClass="form-control" MaxLength="25"></asp:TextBox>
        </div>
        <asp:RequiredFieldValidator ControlToValidate="employer_phone" EnableClientScript="false" Display="Dynamic" CssClass="text-danger" ID="cvEmployerPhone" runat="server" ErrorMessage="Employer Phone required" Enabled="false">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="employer_phone" Text="Employer phone" CssClass="control-label col-md-2"></asp:Label>
            <div class="col-md-4"><asp:TextBox ID="employer_phone" runat="server" CssClass="form-control" MaxLength="8"></asp:TextBox>
        </div>

    </div>

     <div class="form-group">
        <asp:RequiredFieldValidator ControlToValidate="nature" EnableClientScript="false" Display="Dynamic" CssClass="text-danger" ID="cvNature" runat="server" ErrorMessage="Nature of business required" Enabled="false">*</asp:RequiredFieldValidator>
        <asp:Label runat="server" AssociatedControlID="nature" Text="Nature of business" CssClass="control-label col-md-2"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="nature" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
        </div>
    </div>

    <hr />
    <div class="form-group">
            <h4>Disclosures and Regulatory Information</h4>
    </div>
    <div class="form-group">
        <asp:Label AssociatedControlID="employed_by_dealer" runat="server" CssClass="col-md-4 control-label" Text="Are you employed by a registered securities broker/dealer, investment advisor, bank or other financial institution?"></asp:Label>
        <asp:CustomValidator ID="cvEmployedByDealer" runat="server" ControlToValidate="employed_by_dealer" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvEmployedByDealer_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-8">
            <asp:DropDownList ID="employed_by_dealer" CssClass="form-control" runat="server">
                <asp:ListItem Value="0">-Select Answer-</asp:ListItem>
                <asp:ListItem Value="yes">Yes</asp:ListItem>
                <asp:ListItem Value="no">No</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">
        <asp:Label AssociatedControlID="is_director" runat="server" CssClass="col-md-4 control-label" Text="Are you a director, 10% shareholder or policy-making officer of a publicly traded company?"></asp:Label>
        <asp:CustomValidator ID="cvIsDirector" runat="server" ControlToValidate="is_director" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvIsDirector_ServerValidate" >*</asp:CustomValidator>
        <div class="col-md-8">
            <asp:DropDownList ID="is_director" CssClass="form-control" runat="server">
                <asp:ListItem Value="0">-Select Answer-</asp:ListItem>
                <asp:ListItem Value="yes">Yes</asp:ListItem>
                <asp:ListItem Value="no">No</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form-group">
        <asp:Label AssociatedControlID="fund_source" runat="server" CssClass="col-md-4 control-label" Text="Are you employed by a registered securities broker/dealer, investment advisor, bank or other financial institution?"></asp:Label>
        <asp:CustomValidator ID="cvFundSource" runat="server" ControlToValidate="fund_source" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvFundSource_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="fund_source" CssClass="form-control" runat="server" OnSelectedIndexChanged="fund_source_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Value="0">-Select Answer-</asp:ListItem>
                <asp:ListItem Value="salary/wages/savings">Salary/Wages/Savings</asp:ListItem>
                <asp:ListItem Value="investment/capital gains">Investment/Capital gains</asp:ListItem>
                <asp:ListItem Value="family/relatives/inheritance">Family/Relatives/Inheritance</asp:ListItem>
                <asp:ListItem Value="other">Other</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="other_fund_source" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
        </div>
    </div> 
    <hr />
    <div class="form-group">
            <h4>Investment Profile</h4>
    </div>


    <div class="form-group">

        <asp:Label runat="server" Text="Investment objective for this account:" CssClass="control-label col-md-2"></asp:Label>
        <asp:CustomValidator ID="cvDdlObjctivesver" runat="server" ControlToValidate="ddlObjctivesver" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvDdlObjctivesver_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlObjctivesver" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                    <asp:ListItem Value="preservation">capital preservation</asp:ListItem>
                    <asp:ListItem Value="income">income</asp:ListItem>
                    <asp:ListItem Value="growth">growth</asp:ListItem>
                    <asp:ListItem Value="speculation">speculation</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Investment knowledge:" CssClass="control-label col-md-2"></asp:Label>
        <asp:CustomValidator ID="cvDdlKnowledge" runat="server" ControlToValidate="ddlKnowledge" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvDdlKnowledge_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlKnowledge" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                    <asp:ListItem Value="none">none</asp:ListItem>
                    <asp:ListItem Value="limited">limited</asp:ListItem>
                    <asp:ListItem Value="good">good</asp:ListItem>
                    <asp:ListItem Value="extensive">extensive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Investment experience:" CssClass="control-label col-md-2"></asp:Label>
        <asp:CustomValidator ID="cvDdlExperience" runat="server" ControlToValidate="ddlExperience" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvDdlExperience_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlExperience" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                    <asp:ListItem Value="none">none</asp:ListItem>
                    <asp:ListItem Value="limited">limited</asp:ListItem>
                    <asp:ListItem Value="good">good</asp:ListItem>
                    <asp:ListItem Value="extensive">extensive</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Annual income:" CssClass="control-label col-md-2"></asp:Label>
        <asp:CustomValidator ID="cvDdlIncome" runat="server" ControlToValidate="ddlIncome" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvDdlIncome_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlIncome" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                    <asp:ListItem Value="20000<">under HK$20,000</asp:ListItem>
                    <asp:ListItem Value="20001-200000">HK$20,001 - HK$200,000</asp:ListItem>
                    <asp:ListItem Value="200001-2000000">HK$200,001 - HK$2,000,000</asp:ListItem>
                    <asp:ListItem Value=">2000000">more than HK$2,000,000</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">

        <asp:Label runat="server" Text="Approximate liquid net worth (cash and securities):" CssClass="control-label col-md-2"></asp:Label>
        <asp:CustomValidator ID="cvDdlNetworth" runat="server" ControlToValidate="ddlNetworth" ErrorMessage="At least one required questions are unanswered" Display="Dynamic" EnableClientScript="false" CssClass="text-danger" OnServerValidate="cvDdlNetworth_ServerValidate">*</asp:CustomValidator>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlNetworth" runat="server" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">-Please Select-</asp:ListItem>
                    <asp:ListItem Value="100000<">under HK$100,000</asp:ListItem>
                    <asp:ListItem Value="100001-1000000">HK$100,001 - HK$1,000,000</asp:ListItem>
                    <asp:ListItem Value="1000001-10000000">HK$1,000,001 - HK$10,000,000</asp:ListItem>
                    <asp:ListItem Value=">10000000">more than HK$10,000,000</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <div class="form-group">
        <asp:Label runat="server" Text="Intial Account Deposit" CssClass="control-label col-md-4"></asp:Label>
        <div class="col-md-4">
            <asp:TextBox ID="balance" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

    </div>

    <div class="form-group">
        <!-- extra fields to fill the balance item in Account table -->

        <div class="col-md-offset-2 col-md-10"><asp:Button ID="Register" runat="server" Text="Register" CssClass="btn button-default" OnClick="Register_Click"/></div>
    </div>
    </div>

</asp:Content>
