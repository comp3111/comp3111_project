﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using System.Text.RegularExpressions;

namespace HKeInvestWebApplication.EmployeeOnly
{
    public partial class ClientInformation : System.Web.UI.Page
    {
        string[] accountControlIDs = { "ddlAccountType", "AccountNumber","balance"};
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();
        Regex hkidRegex = new Regex(@"^[A-Z]([0-9]{7}|[0-6]{6}\([0-9]\))$");
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void cvAccountNumber_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = true;
            string input = AccountNumber.Text.Trim();
            if(input.Length != 10)
            {
                args.IsValid = false;
                cvAccountNumber.ErrorMessage = "Account number must be of length 10.";
                return;
            }

            if(input.Substring(0,2) != LastName.Text.Trim().ToUpper().Substring(0,2))
            {
                args.IsValid = false;
                cvAccountNumber.ErrorMessage = "First 2 letters of the account number must be the same as the first 2 letters of the last name.";
                return;
            }

            foreach(char c in input.Substring(2))
            {
                if(!Char.IsDigit(c))
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "The last 8 letters of the account number must be digits.";
                    return;
                }
            }
        }

        protected void Register_Click(object sender, EventArgs e)
        {
            Error.Visible = false;
            if (!Page.IsValid) return;
            List<Control> controlList = new List<Control>();
            GetAllControls<DropDownList>(this, controlList);
            GetAllControls<TextBox>(this, controlList);
            commitTransaction(constructInsertSQL(controlList, accountControlIDs, "Account", false));
            commitTransaction(constructInsertSQL(controlList, accountControlIDs, "Client", true));

            emptyAllControls();
            Error.Visible = true;
            Error.Text = "Client information has been saved to the database successfully!";
        }

        private bool isAnAccountControl(Control c)
        {
            return accountControlIDs.Contains(c.ID);
        }
        private void commitTransaction(string sql)
        {
            SqlTransaction trans = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, trans);
            myHKeInvestData.commitTransaction(trans);
        }
        private Control getControlByID(ControlCollection collection,string id)
        {
            if (collection.Count == 0) return null;
            foreach(Control control in collection)
            {
                if (control.ID == id)
                    return control;
                Control c = getControlByID(control.Controls,id);
                if (c != null)
                    return c;
            }
            return null;
        }

        // fill the oldCollection with all control of type controlType included in collection recursively
        private void GetAllControls<ControlType>(Control container,List<Control> ControlList)
        {
            foreach (Control c in container.Controls)
            {
                GetAllControls<ControlType>(c,ControlList);
                if (c is ControlType) ControlList.Add(c);
            }
        }

        private string getCorrespondingSQLEntry(string controlID)
        {

            string result = controlID;
            if (controlID == null || controlID.Length == 0)
                return null;
            if (controlID.Length > 3)
            {
                if (controlID.Substring(0, 3) == "ddl")
                    result = result.Substring(3);
            }
            if (controlID == "HKID")
            {
                return "HKIDPassportNumber";
            }
            if (controlID == "fund_source")
            {
                // so the fund_source should be filled with "other" reason. Now do not insert this field.
                if (fund_source.SelectedValue.Trim() == "other" && other_fund_source.Text.Trim().Length != 0)
                {
                    return null;
                }
                // otherwise just return to the normal case
            }
            if (controlID == "other_fund_source")
            {
                // direct the result to the correct field
                if (fund_source.SelectedValue.Trim() == "other" && other_fund_source.Text.Trim().Length != 0)
                {
                    return "fund_source";
                }else
                {
                    return null;
                }
            }
            if(Char.IsUpper(result[0]))
            {
                result = Char.ToLower(result[0]) + result.Substring(1);
            }
            return result;
        }
        private string constructInsertSQL(List<Control> controlList, string[] controlIDs,string tableName, bool inverseSelection)
        {
            string sql = "insert into [" + tableName + "] (",
            endsql = "values (";

            int controlCount = 0;

            if (!inverseSelection)
                controlCount = accountControlIDs.Length;
            else
            {
                // add one because control
                controlCount = controlList.Count - accountControlIDs.Length;
            }

            int count = 0;

            foreach(Control control in controlList)
            {
                bool shouldInclude = controlIDs.Contains(control.ID);
                if (inverseSelection) shouldInclude = !shouldInclude;
                // accountNumber exists in both tables
                if (control.ID == "AccountNumber") shouldInclude = true;
                if (shouldInclude)
                {
                    sql = sql + getCorrespondingSQLEntry(control.ID);
                    //handle special cases
                    if(control.ID == "fund_source")
                    {
                        DropDownList ddl = control as DropDownList;
                        if (ddl.SelectedValue.Trim() == "other")
                        {
                            continue;
                        }
                        // otherwise it's just an normal case
                    }
                    if(control.ID == "other_fund_source")
                    {
                        TextBox tb = control as TextBox;
                        string value = tb.Text.Trim();
                        
                        if(value.Length != 0 && fund_source.SelectedValue.Trim() == "other")
                        {
                            // do nothing :)
                            // just wait for the code below to do the work
                            //endsql += "'" + other_fund_source.Text.Trim() + "'";
                        }else
                        {
                            continue;
                        }
                    }
                    // resolve control type to retrieve the content inside
                    if (control is TextBox)
                    {
                        TextBox box = control as TextBox;
                        string value = box.Text.Trim();
                        if(control.ID == "dateOfBirth")
                        {
                            endsql += "convert(date,'" + value + "')";
                            
                        }
                        else if(control.ID == "balance")
                        {
                            endsql += balance.Text.Trim();
                        }
                        // handle empty field
                        else if (value.Length != 0)
                            endsql += "'" + box.Text.Trim() + "'";
                        else
                            endsql += "NULL";
                    } else if (control is DropDownList)
                    {
                        DropDownList ddl = control as DropDownList;
                        string value = ddl.SelectedValue.Trim();
                        // handle empty field
                        if (value != "0")
                            endsql += "'" + ddl.SelectedValue.Trim() + "'";
                        else
                            endsql += "NULL";
                    } else
                    {
                        // something wrong there... 
                        // the control is neither TextBox nor DropDownList
                        // which is not supposed to happen
                        return null;
                    }

                    // resolve the comma issue
                    if (count != controlCount - 1)
                    {
                        sql += ",";
                        endsql += ",";
                    }else
                    {
                        sql += ") ";
                        endsql += ") ";
                    }
                    count++;
                }
            } // foreach
           
            return sql + endsql;
        }

        private void emptyAllControls()
        {
            List<Control> controls = new List<Control>();
            GetAllControls<DropDownList>(this,controls);
            foreach(DropDownList ddl in controls)
            {
                ddl.SelectedIndex = 0;
            }
            controls.Clear();
            GetAllControls<TextBox>(this, controls);
            foreach(TextBox tb in controls)
            {
                tb.Text = "";
            }

        }
        protected void phoneValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !(home_phone.Text.Trim().Length == 0 && business_phone.Text.Trim().Length == 0 && mobile_phone.Text.Trim().Length == 0);
            if(!args.IsValid)
            {
                phoneValidator.ErrorMessage = "At least one of the phone numbers required.";
            }
        }

        protected void AccountType_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = ddlAccountType.SelectedValue != "0";
        }

        protected void fund_source_SelectedIndexChanged(object sender, EventArgs e)
        {
            string opt = fund_source.SelectedValue.Trim();
            other_fund_source.Visible = (opt == "other");
        }

        protected void cvEmployedByDealer_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = employed_by_dealer.SelectedValue.Trim() != "0";
        }

        protected void cvIsDirector_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = is_director.SelectedValue.Trim() != "0";
        }

        protected void cvFundSource_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string opt = fund_source.SelectedValue.Trim();
            args.IsValid = true;
            if(opt == "0")
            {
                args.IsValid = false;
            }
            if(opt == "other" && other_fund_source.Text.Trim().Length == 0)
            {
                args.IsValid = false;
            }

        }
        // check if the account number has been registered before

        protected void cvDdlObjctivesver_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (ddlObjctivesver.SelectedValue.Trim() != "0");
        }

        protected void cvDdlKnowledge_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (ddlKnowledge.SelectedValue.Trim() != "0");
        }

        protected void cvDdlExperience_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (ddlExperience.SelectedValue.Trim() != "0");
        }

        protected void cvDdlIncome_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (ddlIncome.SelectedValue.Trim() != "0");
        }

        protected void cvDdlNetworth_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = (ddlNetworth.SelectedValue.Trim() != "0");
        }

        protected void cvPassport_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string hkid_passport = HKID.Text.Trim();
            
            
            if(hkidRegex.IsMatch(hkid_passport))
            {
                // the input in HKID field is indeed a HKID 
                args.IsValid = true;
            }else
            {
                // it it's not then its a passport number
                args.IsValid = (passport_country_of_issue.Text.Trim().Length != 0);
            }
        }

        protected void HKID_TextChanged(object sender, EventArgs e)
        {
            string input = HKID.Text.Trim();
            if(new Regex(@"^[A-Z][0-9]{6}\([0-9]\)").IsMatch(input))
            {
                // the input is a HKID with format A123456(7)
                // convert the input to the format A1234567
                // so that the input will be consistent across the database
                HKID.Text = input.Substring(0, 7) + input[8];
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            // toggle enable of relevant validators
            bool shouldEnable = (ddlStatus.SelectedValue.Trim() == "employed");
            cvEmployerName.Enabled = shouldEnable;
            cvEmployerPhone.Enabled = shouldEnable;
            cvYears.Enabled = shouldEnable;
            cvSpecificOccupation.Enabled = shouldEnable;
            cvNature.Enabled = shouldEnable;
        }
    }


}