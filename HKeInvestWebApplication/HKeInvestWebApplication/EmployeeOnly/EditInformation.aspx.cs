﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HKeInvestWebApplication.Code_File;
using System.Data;
using System.Data.SqlClient;

namespace HKeInvestWebApplication.EmployeeOnly
{
 
    public partial class EditInformation : System.Web.UI.Page
    {
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        HKeInvestData myHKeInvestData = new HKeInvestData();

        List<Control> accountControlIDs = new List<Control>();
        List<Control> clientControlIDs = new List<Control>();

        string selectedAccountNumber = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            // group all controls to their places for easier maipulation at later times

            accountControlIDs.Add(Password);

            clientControlIDs.Add(FirstName);
            clientControlIDs.Add(LastName);
            clientControlIDs.Add(Email);
            clientControlIDs.Add(Building);
            clientControlIDs.Add(Street);
            clientControlIDs.Add(District);
            clientControlIDs.Add(home_phone);
            clientControlIDs.Add(home_fax);
            clientControlIDs.Add(business_phone);
            clientControlIDs.Add(mobile_phone);
            clientControlIDs.Add(country_of_citizenship);
            clientControlIDs.Add(country_of_residence);
            clientControlIDs.Add(ddlExperience);
            clientControlIDs.Add(ddlIncome);
            clientControlIDs.Add(ddlNetworth);
            clientControlIDs.Add(ddlObjctivesver);
            clientControlIDs.Add(ddlStatus);
        }

        protected void AccountNumber_TextChanged(object sender, EventArgs e)
        {
            ErrorMessage.Visible = false;
            InfoPanel.Visible = false;
            string accountNumber = AccountNumber.Text.Trim();
            string sql = "select firstName,lastName from [Client] where accountNumber='" + accountNumber + "'";
            DataTable table = myHKeInvestData.getData(sql);
            if(table == null || table.Rows.Count == 0)
            {
                ErrorMessage.Text = "No such account";
                ErrorMessage.Visible = true;
                return;
            }
            selectedAccountNumber = accountNumber;
            // start loading user name to drop down list
            ClientName.Items.Clear();
            ClientName.Items.Add("--Select Client--");
            foreach (DataRow row in table.Rows)
            {

                ClientName.Items.Add((row["firstName"] as string + " " + row["lastName"]).Trim());
            }
        }

        protected void ClientName_SelectedIndexChanged(object sender, EventArgs e)
        {
            ErrorMessage.Visible = false;
            InfoPanel.Visible = false;
            string[] tokens = ClientName.SelectedValue.Trim().Split();
            string accountNumber = AccountNumber.Text.Trim();
            string firstName = tokens[0];
            string lastName = tokens.Last();
            string sql = "select * from [Client] where accountNumber='" + accountNumber + "' and charindex('" + firstName + "',firstName) > 0 and charindex('" + lastName + "',lastName) > 0";
            DataTable table = myHKeInvestData.getData(sql);
            if (table == null || table.Rows.Count == 0)
            {
                ErrorMessage.Text = "No such user";
                ErrorMessage.Visible = true;
                return;
            }

            // everything's ok, load data table
            DataTable acTable = myHKeInvestData.getData("select * from [Account] where accountNumber='" + accountNumber + "'");
            if (acTable == null || acTable.Rows.Count == 0)
            {
                ErrorMessage.Text = "Error occured when loading information of specified account";
                ErrorMessage.Visible = true;
                return;
            }
            loadDataTable(table.Rows[0]);
            loadAccountTable(acTable.Rows[0]);

            InfoPanel.Visible = true;
        }

        private void loadAccountTable(DataRow info)
        {

        }
        private void loadDataTable(DataRow info)
        {
            foreach(Control control in clientControlIDs)
            {
                string sqlField = getSQLFieldNameByControlID(control.ID);
                if (control is TextBox)
                {
                    TextBox tb = control as TextBox;
                    string content = info[sqlField] as string;
                    if(content != null)
                    {
                        tb.Text = content.Trim();
                    }

                }
            }
        }

        private string getSQLFieldNameByControlID(string id)
        {
            string sqlField = Char.ToLower(id[0]) + id.Substring(1);
            return sqlField;
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            string sql = "update [Client] set ";
            int count = clientControlIDs.Count, i = 0;
            // construct sql based on the info given in the table
            foreach(Control control in clientControlIDs)
            {
                i++;
                if(control is TextBox)
                {
                    TextBox tb = control as TextBox;
                    string content = tb.Text.Trim();

                    if (content.Length == 0)
                    {
                        sql += (getSQLFieldNameByControlID(control.ID) + "=NULL" + ((i == count) ? "" : ","));
                    }else
                    {
                        sql += (getSQLFieldNameByControlID(control.ID) + "='" + content + ((i == count) ? "'" : "',"));
                    }

                }
            }

            // finally...
            string[] tokens = ClientName.SelectedValue.Trim().Split();
            string accountNumber = AccountNumber.Text.Trim();
            string firstName = tokens[0];
            string lastName = tokens.Last();
            sql += " where accountNumber='" + accountNumber + "' and charindex('" + firstName + "',firstName) > 0 and charindex('" + lastName + "',lastName) > 0 ";

            // then submit data...
            SqlTransaction t = myHKeInvestData.beginTransaction();
            myHKeInvestData.setData(sql, t);
            myHKeInvestData.commitTransaction(t);

            // really finally..
            ClientName.Items.Clear();
            AccountNumber.Text = "";
            InfoPanel.Visible = false;
            ErrorMessage.Text = "Client information has been updated!";
            ErrorMessage.Visible = true;
        }
    }
}