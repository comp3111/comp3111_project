﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecurityHoldingDetails.aspx.cs" Inherits="HKeInvestWebApplication.SecurityHoldingDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Security Holding Details</h2>
    <div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="txtAccountNumber" Text="Account number:" CssClass="control-label"></asp:Label>
            <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="form-control" OnTextChanged="txtAccountNumber_TextChanged"></asp:TextBox>
            <asp:DropDownList ID="ddlSecurityType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSecurityType_SelectedIndexChanged" CssClass="form-control">
                <asp:ListItem Value="0">Security type</asp:ListItem>
                <asp:ListItem Value="bond">Bond</asp:ListItem>
                <asp:ListItem Value="stock">Stock</asp:ListItem>
                <asp:ListItem Value="unit trust">Unit Trust</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="ddlCurrency" runat="server" AutoPostBack="True" Visible="False" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged">
                <asp:ListItem Value="0">Currency</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="lblClientName" runat="server" Visible="False" CssClass="control-label"></asp:Label>
            <asp:Label ID="lblResultMessage" runat="server" Visible="False" CssClass="control-label"></asp:Label>
        </div>
        <div class="form-group">
            <asp:GridView ID="gvSecurityHolding" runat="server" AutoGenerateColumns="False" Visible="False"  OnSorting="gvSecurityHolding_Sorting" CssClass="table table-bordered">
                <Columns>
                    <asp:BoundField DataField="code" HeaderText="Code" ReadOnly="True" SortExpression="code" />
                    <asp:BoundField HeaderText="Name" ReadOnly="True" SortExpression="name" DataField="name" />
                    <asp:BoundField DataField="shares" DataFormatString="{0:n2}" HeaderText="Shares" ReadOnly="True" SortExpression="shares" />
                    <asp:BoundField DataField="base" HeaderText="Base" ReadOnly="True" />
                    <asp:BoundField DataField="price" DataFormatString="{0:n2}" HeaderText="Price" ReadOnly="True" />
                    <asp:BoundField DataField="value" DataFormatString="{0:n2}" HeaderText="Value" ReadOnly="True" SortExpression="value" />
                    <asp:BoundField DataField="convertedValue" DataFormatString="{0:n2}" HeaderText="Value in" ReadOnly="True" SortExpression="convertedValue" />
                </Columns>
            </asp:GridView>
        </div>
        <hr />
        <asp:Panel ID="extraPanel" runat="server">
            <div class="form-group">
                <h4> Account Summary </h4>
            </div>
            <div class="form-group">
                <asp:GridView ID="gvSummary" runat="server" Visible="false" CssClass="table table-bordered">
                    <Columns>
                        <asp:BoundField HeaderText="Free Balance (HKD)" ReadOnly="True" DataField="balance"></asp:BoundField>
                        <asp:BoundField HeaderText="Bond Value (HKD)" ReadOnly="True" DataField="bond"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Value (HKD)" ReadOnly="True" DataField="stock"></asp:BoundField>
                        <asp:BoundField HeaderText="Unit Trust Value (HKD)" ReadOnly="True" DataField="unit trust"></asp:BoundField>
                        <asp:BoundField HeaderText="Total Value (HKD)" ReadOnly="True" DataField="total"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <hr /> <!-- active orders-->
            <div class="form-group">
                <h4> Active Orders </h4>
            </div>

            <div class="form-group">
                <asp:Label ID="lblActiveOrders" runat="server" Visible="false" Text="No Active Order placed by this account."></asp:Label>
                <asp:GridView ID="gvActiveOrders" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Date Submitted" ReadOnly="True" DataField="dateSubmitted"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Type" ReadOnly="True" DataField="securityType"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Code" ReadOnly="True" DataField="securityCode"></asp:BoundField>
                        <asp:BoundField HeaderText="Buy/Sell Order" ReadOnly="True" DataField="buyOrSell"></asp:BoundField>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Name" ReadOnly="True" DataField="securityName"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Status" ReadOnly="True" DataField="status"></asp:BoundField>

                        <asp:BoundField HeaderText="Dollar Amount(HKD)" ReadOnly="True" DataField="amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of Shares" ReadOnly="True" DataField="shares"></asp:BoundField>

                        <asp:BoundField HeaderText="Expiry Date" ReadOnly="True" DataField="expiryDay"></asp:BoundField>

                        <asp:BoundField HeaderText="Selling Price(HKD)" ReadOnly="True" DataField="limitPrice"></asp:BoundField>

                        <asp:BoundField HeaderText="Stop Price (HKD)" ReadOnly="True" DataField="stopPrice"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Order Type" ReadOnly="true" DataField="stockOrderType"></asp:BoundField>
                        <asp:BoundField HeaderText="Account Number" ReadOnly="true" DataField="accountNumber" Visible="false"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <hr /> <!--Order history-->
            <div class="form-group">
                <h4> Order history </h4>
            </div>
            <div class="form-group">
                
                <asp:Label AssociateControlID="fromDate" runat="server" Text="Order From " CssClass="control-label col-md-3"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="fromDate" runat="server" CssClass="form-control" OnTextChanged="fromDate_TextChanged"></asp:TextBox>
                    </div>
                <asp:Label AssociateControlID="toDate" runat="server" Text="To" CssClass="control-label col-md-3"></asp:Label>
                    <div class="col-md-3">
                        <asp:TextBox ID="toDate" runat="server" CssClass="form-control" OnTextChanged="toDate_TextChanged"></asp:TextBox>
                    </div>

            </div>
            <div class="form-group">
                <asp:Label ID="lblOrderHistory" runat="server" Visible="false" Text="No Active Order placed by this account."></asp:Label>
                <asp:GridView ID="gvOrderHistory" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Date Submitted" ReadOnly="True" DataField="dateSubmitted"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Type" ReadOnly="True" DataField="securityType"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Code" ReadOnly="True" DataField="securityCode"></asp:BoundField>
                        <asp:BoundField HeaderText="Buy/Sell Order" ReadOnly="True" DataField="buyOrSell"></asp:BoundField>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Security Name" ReadOnly="True" DataField="securityName"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Status" ReadOnly="True" DataField="status"></asp:BoundField>

                        <asp:BoundField HeaderText="Dollar Amount(HKD)" ReadOnly="True" DataField="amount"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of Shares" ReadOnly="True" DataField="shares"></asp:BoundField>

                        <asp:BoundField HeaderText="Expiry Date" ReadOnly="True" DataField="expiryDay"></asp:BoundField>

                        <asp:BoundField HeaderText="Selling Price(HKD)" ReadOnly="True" DataField="limitPrice"></asp:BoundField>

                        <asp:BoundField HeaderText="Stop Price (HKD)" ReadOnly="True" DataField="stopPrice"></asp:BoundField>
                        <asp:BoundField HeaderText="Stock Order Type" ReadOnly="true" DataField="stockOrderType"></asp:BoundField>
                        <asp:BoundField HeaderText="Account Number" ReadOnly="true" DataField="accountNumber" Visible="false"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="form-group">
                <asp:Label ID="lblTransaction" runat="server" Visible="false" Text="No transactions associated to this account"></asp:Label>
                <asp:GridView ID="gvTransaction" runat="server" Visible="false" CssClass="table table-bordered" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField HeaderText="Reference Number" ReadOnly="True" DataField="referenceNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Order Reference Number" ReadOnly="True" DataField="orderNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="Date Executed" ReadOnly="True" DataField="dateExecuted"></asp:BoundField>
                        <asp:BoundField HeaderText="Quantity of share" ReadOnly="True" DataField="quantity"></asp:BoundField>
                        <asp:BoundField HeaderText="Price per share" ReadOnly="True" DataField="pricePerShare"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:Panel>
        <hr />
    </div>
</asp:Content>

