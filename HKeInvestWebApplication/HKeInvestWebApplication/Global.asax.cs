﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;
using System.Net;
using System.Net.Mail;
using HKeInvestWebApplication.Code_File;
using System.Data;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using System.Data.Sql;
using System.Data.SqlClient;
namespace HKeInvestWebApplication
{
    public class Global : HttpApplication
    {
        SecuritiesPortfolioManager portfolioManager;
        HKeInvestData data = new HKeInvestData();
        HKeInvestCode code = new HKeInvestCode();
        ExternalFunctions external = new ExternalFunctions();
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // get the portfolioM
            portfolioManager = SecuritiesPortfolioManager.getSecuritesPortfolioManager();
            // start running the portfolio manager
            portfolioManager.run();

            Thread thread = new Thread(PeriodicTask);
            thread.IsBackground = true;
            thread.Start();
        }
        enum AlertTriggerType
        {
            High,Low,NoTrigger
        }
        // keep checking alert
        private void PeriodicTask()
        {
            do
            {

                // so that the date should be 
                DataTable table = data.getData("select * from [Alert] where not lastTriggerDate = dateadd(day,datediff(day,0,GETDATE()),0) or lastTriggerDate is NULL");
                if (table == null) return;
                foreach(DataRow alertRow in table.Rows)
                {
                    string triggerReason = null;
                    switch (shouldTriggerAlert(alertRow))
                    {
                        case AlertTriggerType.NoTrigger: continue;
                        case AlertTriggerType.High:
                            triggerReason = "upper bound value is reached.";
                            break;
                        case AlertTriggerType.Low:
                            triggerReason = "lower bound value is reached.";
                            break;
                    }
                    string title = "HKeInvest alert";
                    string c = (alertRow["code"] as string).Trim();
                    
                    // get the rest of the data from the info of the external system
                    DataTable row_data = external.getSecuritiesData(c);
                    string type = code.getSecurityTypeByCode(c);
                    string name = code.getSecurityNameByCode(c);
                    decimal price = external.getSecuritiesPrice(type,c);

                    string content = String.Format("The alert you set for {0} name {1} with code {2} is triggered at price {3} because the ",type,name,c,price) + triggerReason;
                    //code.sendEmail(getEmailOfClientOfAlert(alertRow), title, content);
                    updateTriggerDate(alertRow);
                }
                Thread.Sleep(1000);
            }
            while (true);
        }
        private string getEmailOfClientOfAlert(DataRow row)
        {
            string accountNumber = (row["accountNumber"] as string).Trim();
            DataTable table = data.getData(String.Format("select email from [Client] where accountNumber='{0}'", accountNumber));
            if (table == null || table.Rows.Count == 0) return null; // strange though
            return (table.Rows[0]["email"] as string).Trim();
        }
        private void updateTriggerDate(DataRow alertRow)
        {
            string sql = String.Format("update [Alert] set lastTriggerDate= GETDATE() where highvalue='{0}' and lowvalue='{1}' and accountNumber='{2}'", alertRow["highvalue"], alertRow["lowvalue"], alertRow["accountNumber"]);
            SqlTransaction t = data.beginTransaction();
            data.setData(sql, t);
            data.commitTransaction(t);
        }

        private AlertTriggerType shouldTriggerAlert(DataRow alertRow)
        {
            string s_code = (alertRow["code"] as string).Trim();
            string type = code.getSecurityTypeByCode(s_code);
            decimal high = Convert.ToDecimal(alertRow["highvalue"]);
            decimal low = Convert.ToDecimal(alertRow["lowvalue"]);
            decimal price = external.getSecuritiesPrice(type, s_code);
            if (price >= high) return AlertTriggerType.High;
            if (price <= low) return AlertTriggerType.Low;
            return AlertTriggerType.NoTrigger;
        }

    }
}