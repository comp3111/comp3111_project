﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HKeInvestWebApplication.Code_File;
using System.Data;

using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HKeInvestWebApplication
{
    public partial class RegistrationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        protected void cvAccountNumber_ServerValidate(object source, ServerValidateEventArgs args)
        {
            string account_number = AccountNumber.Text.Trim();
            string last_name = LastName.Text.Trim();
            string lastname = last_name.ToString().ToUpper();
            char first_letter;
            char second_letter;

            if (lastname != "")
            {
                first_letter = lastname[0];
                second_letter = lastname[1];

                if (account_number.Contains(first_letter) == false || (account_number.Contains(first_letter) && account_number.Contains(second_letter)) == false)
                {
                    args.IsValid = false;
                    cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";
                }
                else
                {
                    foreach (char c in account_number.Substring(2, 9))
                    {
                        if (c < '0' || c > '9')
                        {
                            args.IsValid = false;
                            cvAccountNumber.ErrorMessage = "The account number does not match the client's last name.";

                        }

                    }
                }
            }

            

        }

        protected void Register_Click(object sender, EventArgs e)
        {
            SubmitErrorLabel.Visible = false;
            HKeInvestData data = new HKeInvestData();
            string sql = "select * from Client where accountNumber='" + AccountNumber.Text.Trim() + "'";
            DataTable table = data.getData(sql);
            if(table == null)
            {
                SubmitErrorLabel.Visible = true;
                SubmitErrorLabel.Text = "Unable to connect to the server";
            }
            if(table.Rows.Count == 0)
            {
                SubmitErrorLabel.Visible = true;
                SubmitErrorLabel.Text = "Input data does not match with the method";
            }else
            {
                foreach(DataRow row in table.Rows)
                {
                    if (row["firstName"].ToString().Trim().ToLower() == FirstName.ToString().Trim().ToLower()
                        && row["lastName"].ToString().Trim().ToLower() == LastName.ToString().Trim().ToLower()
                        && row["email"].ToString().Trim().ToLower() == Email.ToString().Trim().ToLower()
                        && row["dateOfBirth"].ToString().Trim().ToLower() == DateOfBirth.ToString().Trim().ToLower()
                        && row["HKIDPassportNumber"].ToString().Trim().ToLower() == HKID.ToString().Trim().ToLower())
                    {
                        //TODO: create account in IdentityManager

                    }else
                    {
                        SubmitErrorLabel.Visible = true;
                        SubmitErrorLabel.Text = "Input data does not match with the method";
                    }
                }
            }
        }
    }
}