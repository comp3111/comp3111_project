﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using HKeInvestWebApplication.Code_File;
using HKeInvestWebApplication.ExternalSystems.Code_File;
using Microsoft.AspNet.Identity;

namespace HKeInvestWebApplication
{
    public partial class SecuritySearch : System.Web.UI.Page
    {
        string selectedSecurityType = null;
        HKeInvestData myHKeInvestData = new HKeInvestData();
        HKeInvestCode myHKeInvestCode = new HKeInvestCode();
        ExternalFunctions myExternalFunctions = new ExternalFunctions();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SecurityType_changed(object sender,EventArgs e)
        {
            //hide the rest of the fields first
            SecurityCode.Visible = false;
            SecurityCodeLabel.Visible = false;
            SecurityName.Visible = false;
            SecurityNameLabel.Visible = false;

            //get the selected value
            selectedSecurityType = SecurityType.SelectedValue;
       
            renderTable(null,null);

            //provide extra options if the selected index is not null
            if(selectedSecurityType != "0")
            {
                SecurityCode.Visible = true;
                SecurityName.Visible = true;
                SecurityCodeLabel.Visible = true;
                SecurityNameLabel.Visible = true;
            }
        }

        protected void SecurityName_changed(object sender,EventArgs e)
        {
            //Empty the code field
            SecurityCode.Text = "";

            selectedSecurityType = SecurityType.SelectedValue;
            string name = SecurityName.Text.Trim().ToLower();
            renderTable(name, null);
        }
        
        protected void SecurityCode_changed(object sender, EventArgs e)
        {
            //Empty the name field
            SecurityName.Text = "";
            selectedSecurityType = SecurityType.SelectedValue;
            string code = SecurityCode.Text.Trim();
            renderTable(null, code);
        }
        protected void ResultGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dtSecurityHolding = (DataTable)ViewState["dataSource"];
            if(dtSecurityHolding == null)
            {
                return;
            }
            // Set the sort expression in ViewState for correct toggling of sort direction,
            // Sort the DataTable and bind it to the GridView.
            string sortExpression = e.SortExpression.ToLower();
            ViewState["SortExpression"] = sortExpression;
            dtSecurityHolding.DefaultView.Sort = sortExpression + " " + myHKeInvestCode.getSortDirection(ViewState, e.SortExpression);
            dtSecurityHolding.AcceptChanges();

            // Bind the DataTable to the GridView.
            ResultGridView.DataSource = dtSecurityHolding.DefaultView;
            ResultGridView.DataBind();
        }
        //helper function that renders the content inside a given table to grid view
        //this function does nothing if the table is null
        protected void renderTable(string securityName, string code)
        {
            // suppress message label first
            MessageLabel.Text = "";
            MessageLabel.Visible = false;

            setVisibilityBySelectedType();
            //suppress the grid view as well

            ResultGridView.Visible = false;

            // check if parameter is valid
            if (selectedSecurityType == null) return;


            // get the data from server
            DataTable table = myExternalFunctions.getSecuritiesData(selectedSecurityType);
            if (code == null && securityName == null)
            {
                ResultGridView.Visible = true;
                setVisibilityBySelectedType();
                ResultGridView.DataSource = table;
                ResultGridView.DataBind();

                ViewState["dataSource"] = table;
                return;
            }
            // this table get the filtered result according to code or name
            DataTable resultTable = createTable();

            if (securityName == null)
            {
                //search according to given code

                //if the code is not 4-digits long
                if (code.Length > 4 || code.Length == 0) return;

                foreach(DataRow row in table.Rows)
                {
                    // the entry matches the string
                    if(row["code"].ToString().Trim().ToLower() == code)
                    {
                        resultTable.Rows.Add(row.ItemArray);
                    }
                }
            }
            else
            {
                //search according to given name, which can be part of the name
                foreach (DataRow row in table.Rows)
                {
                    // the entry matches the string
                    if (row["name"].ToString().Trim().ToLower().Contains(securityName))
                    {
                        resultTable.Rows.Add(row.ItemArray);
                    }
                }
            }

            //finally..
            if(resultTable.Rows.Count == 0)
            {
                //do nothing if nothing matches the search
                //inform user about this.
                MessageLabel.Visible = true;
                return;
            }
            //otherwise
            ResultGridView.Visible = true;
            ResultGridView.DataSource = resultTable;
            ResultGridView.DataBind();

            // stuff related to sorting...
            ViewState["SortExpression"] = "name";
            ViewState["SortDirection"] = "ASC";

            //finally.. save the dataTable for future uses...
            ViewState["dataSource"] = resultTable;
        }
        // small helper function that actually adds the value of the entry to the view
        // it takes the selectedSecurityType string directly from the class member.
        protected DataTable createTable()
        {
            DataTable table = new DataTable();
            if(selectedSecurityType == "stock")
            {
                string[] attrs = { "code", "name", "close", "changePercent", "changeDollar", "volume", "high", "low", "peRatio", "yield" };
                foreach(string attr in attrs)
                {
                    table.Columns.Add(attr);
                }
                ViewState["columns"] = attrs;
            }else if(selectedSecurityType == "bond")
            {
                string[] attrs = { "code", "name", "launchDate", "base", "size", "price", "sixMonths", "oneYear", "threeYears", "sinceLaunch" };
                foreach (string attr in attrs)
                {
                    table.Columns.Add(attr);
                }
                ViewState["columns"] = attrs;
            }else if(selectedSecurityType == "unit trust")
            {
                string[] attrs = { "code", "name", "launchDate", "base", "size", "price", "riskReturn", "sixMonths", "oneYear", "threeYears", "sinceLaunch"};
                foreach (string attr in attrs)
                {
                    table.Columns.Add(attr);
                }
                ViewState["columns"] = attrs;
            }else
            {
                return null;
            }

            return table;
        }

        protected void setAllColumnsToInvisible()
        {
            for(int i = 0; i < ResultGridView.Columns.Count; i++)
            {
                ResultGridView.Columns[i].Visible = false;
            }
        }

        protected void setVisibilityBySelectedType()
        {
            setAllColumnsToInvisible();
            //check if the createTable can actually creates a table
            DataTable table = createTable();
            if (table == null) return;
            // for getting the column name only
            foreach (DataColumn column in table.Columns)
            {
                int i = myHKeInvestCode.getColumnIndexByName(ResultGridView,column.ColumnName);
                if (i < 0) return;
                ResultGridView.Columns[i].Visible = true;
            }
        }

    }

}